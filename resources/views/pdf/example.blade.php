{{--  <!DOCTYPE html>
 <html lang="en">
 <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <style>

        .table{
            width: 100%;
        }
        .table thead{
            font-size: 20px;
        }
        .column {
            float: left;
            width: 33.33%;
        }
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
    </style>
 </head>
 <body>
    <div class="container-fluid">
        <div class="card">
            
            <div class="card-header mb-5">
               <div class="row">
                    <div class="column text-center" >
                        <img src="{{ asset('img/dmmmsu_logo.png') }}" alt="" style="width: 120px" height="110px;">
                    </div>
                    <div class="column text-center">
                        <h1 class="text-center">
                            University Board<br>of Secretaries Office (UBSO) - DMS
                        </h1>
                    </div>
                    <div class="column text-center">
                        <img src="{{ asset('img/Bagong_Pilipinas_logo.png') }}" alt="" style="width: 110px" height="110px;">
                    </div>
                </div>
            </div>

            <div class="card-body">
                <table class="table" >
                    <thead>
                        @if ($action == 1 || $action == 2)
                            <tr>
                                <td><strong>Category</strong></td>
                                <td style=' text-align: center;'><strong>Total Meeting</strong></td>
                            </tr>
                        @elseif ($action == 3)
                            <tr>
                                <td><strong>Category</strong></td>
                                <td style=' text-align: center;'><strong>Year</strong></td>
                                <td style=' text-align: center;'><strong>Total Meeting</strong></td>
                            </tr>
                        @endif
                        
                    </thead>
                    <tbody>
                        @if ($action == 1)
                            @foreach ($category as $categories)
                                <tr>
                                    <td>{{ $categories->categoryName }}</td>
                                    @if($categories->categoryName == 'Academic Council')
                                        <td style=' text-align: center;'>{{ $adt }}</td>
                                    @elseif ($categories->categoryName == 'Admin Council')
                                        <td style=' text-align: center;'>{{ $act }}</td>
                                    @elseif ($categories->categoryName == 'Preboard Meeting')
                                        <td style=' text-align: center;'>{{ $pbmm }}</td>
                                    @elseif ($categories->categoryName == 'Regular Board Meeting')
                                        <td style=' text-align: center;'>{{ $rbmm }}</td>
                                    @elseif ($categories->categoryName == 'Special Meeting')
                                        <td style=' text-align: center;'>{{ $spm }}</td>
                                    @else
                                        
                                    @endif
                                </tr>
                            @endforeach
                            <tr>
                                <td><strong>Total Meeting</strong></td>
                                <td style=' text-align: center;'>{{ $total }}</td>
                            </tr>
                        @elseif ($action == 2)
                           @foreach ($total AS $totals => $t)
                                <tr>
                                    <td>{{ $t['category'] }}</td>
                                    <td style=' text-align: center;'>{{ $t['count'] }}</td>
                                </tr>
                           @endforeach
                        @elseif ($action == 3)
                            @foreach ($total AS $totals => $t)
                                <tr>
                                    <td>{{ $t['category'] }}</td>
                                    <td style=' text-align: center;'>{{ $t['year'] }}</td>
                                    <td style=' text-align: center;'>{{ $t['count'] }}</td>
                                </tr>
                           @endforeach
                        @endif
                        
                    </tbody>
        
               </table>
            </div>
        </div>
    </div>
        
       <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
 </body>
 </html> --}}
 <!DOCTYPE html>
 <html lang="en">
 <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
   
 </head>
 <body>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
       
        <style>
    
            .table{
                width: 100%;
            }
            .table thead{
                font-size: 20px;
            }
            .column {
                float: left;
                width: 33.33%;
            }
            .row:after {
                content: "";
                display: table;
                clear: both;
            }
        </style>
     </head>
     <body>
        <table>
            <tbody>
                <tr>
                    <td>{{-- <img src="{{ asset('img/dmmmsu_logo.png') }}" alt="" style="width:50px; height: 50px;"> --}}
                        <img src="{{ 'data:image/png;base64,' . base64_encode(file_get_contents(public_path('img/dmmmsu_logo.png'))) }}"
    width="180" />
                    </td>
                    <td style="text-align: center;">
                        <h1>
                            University Board<br>of Secretaries Office (UBSO) - DMS
                        </h1>
                    </td>
                    <td>
                        {{-- <img src="{{ asset('img/Bagong_Pilipinas_logo.png') }}" alt="" style="width:50px; height: 50px;"> --}}
                        <img src="{{ 'data:image/png;base64,' . base64_encode(file_get_contents(public_path('img/Bagong_Pilipinas_logo.png'))) }}"
    width="180" />
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="table" >
            <thead>
                @if ($action == 1 || $action == 2)
                    <tr>
                        <td><strong>Category</strong></td>
                        <td style=' text-align: center;'><strong>Total Meeting</strong></td>
                    </tr>
                @elseif ($action == 3)
                    <tr>
                        <td><strong>Category</strong></td>
                        <td style=' text-align: center;'><strong>Year</strong></td>
                        <td style=' text-align: center;'><strong>Total Meeting</strong></td>
                    </tr>
                @endif
                
            </thead>
            <tbody>
                @if ($action == 1)
                    @foreach ($category as $categories)
                        <tr>
                            <td>{{ $categories->categoryName }}</td>
                            @if($categories->categoryName == 'Academic Council')
                                <td style=' text-align: center;'>{{ $adt }}</td>
                            @elseif ($categories->categoryName == 'Admin Council')
                                <td style=' text-align: center;'>{{ $act }}</td>
                            @elseif ($categories->categoryName == 'Preboard Meeting')
                                <td style=' text-align: center;'>{{ $pbmm }}</td>
                            @elseif ($categories->categoryName == 'Regular Board Meeting')
                                <td style=' text-align: center;'>{{ $rbmm }}</td>
                            @elseif ($categories->categoryName == 'Special Meeting')
                                <td style=' text-align: center;'>{{ $spm }}</td>
                            @else
                                
                            @endif
                        </tr>
                    @endforeach
                    <tr>
                        <td><strong>Total Meeting</strong></td>
                        <td style=' text-align: center;'>{{ $total }}</td>
                    </tr>
                @elseif ($action == 2)
                   @foreach ($total AS $totals => $t)
                        <tr>
                            <td>{{ $t['category'] }}</td>
                            <td style=' text-align: center;'>{{ $t['count'] }}</td>
                        </tr>
                   @endforeach
                @elseif ($action == 3)
                    @foreach ($total AS $totals => $t)
                        <tr>
                            <td>{{ $t['category'] }}</td>
                            <td style=' text-align: center;'>{{ $t['year'] }}</td>
                            <td style=' text-align: center;'>{{ $t['count'] }}</td>
                        </tr>
                   @endforeach
                @endif
                
            </tbody>

       </table>

     </body>
 </body>
 </html>