<div>
    <!-- Simplicity is an acquired taste. - Katharine Gerould -->
    <button {{ $attributes->merge(['class' => 'btn btn-success rounded-pill p-2']) }}>
        {{ $slot }}
    </button>
</div>