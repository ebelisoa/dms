<div class="{{ $attributes->merge(['class' => 'd-flex justify-content-end align-items-center']) }}">
    <!-- Nothing in life is to be feared, it is only to be understood. Now is the time to understand more, so that we may fear less. - Marie Curie -->
    {{ $slot }}
</div>