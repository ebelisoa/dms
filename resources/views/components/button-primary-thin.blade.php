<div>
    <!-- The whole future lies in uncertainty: live immediately. - Seneca -->
    <button {{ $attributes->merge(['class' => 'btn btn-success rounded-pill p-1', 'style' => 'width:120px;']) }}>
        {{ $slot }}
    </button>
</div>