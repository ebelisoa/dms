<div>
    <!-- If you do not have a consistent goal in life, you can not live it in a consistent way. - Marcus Aurelius -->
    <button {{ $attributes->merge(['class' => 'btn btn-success rounded-pill p-1', 'style' => 'width:120px; height:40px;']) }}>
        {{ $slot }}
    </button>
</div>