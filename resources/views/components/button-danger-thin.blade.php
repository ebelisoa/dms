<div>
    <!-- The only way to do great work is to love what you do. - Steve Jobs -->
    <button {{ $attributes->merge(['class' => 'btn btn-danger rounded-pill p-1', 'style' => 'width:120px;']) }}>
        {{ $slot }}
    </button>
</div>