<div>
    <!-- Let all your things have their places; let each part of your business have its time. - Benjamin Franklin -->
    <button {{ $attributes->merge(['class' => 'btn btn-danger rounded-pill p-1', 'style' => 'width:120px; height:40px;']) }}>
        {{ $slot }}
    </button>
</div>