<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>UNIBOSS - Document Management System</title>
  <link rel="icon" type="image/x-icon" href="{{ asset('img/dmmmsu_logo.png') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="{{ asset('css/mdb.min.css')}}" rel="stylesheet">
  <!-- Your custom styles (optional) -->

  <link rel="stylesheet" href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

  <style>
    body{
     
        font-family: "Roboto", sans-serif;
        font-weight: 700;
        font-style: normal;
     
    }
    
    html,
    body,
    header,
    .view {
      height: 100%;
    }

    @media (min-width: 560px) and (max-width: 740px) {
      html,
      body,
      header,
      .view {
        height: 650px;
      }
    }

    @media (min-width: 800px) and (max-width: 850px) {
      html,
      body,
      header,
      .view  {
        height: 650px;
      }
    }

    input::placeholder{
      color:white;
    }    
  </style>
</head>

<body class="login-page">
  <div class="container-fluid align-middle">
    <div class="row">

      <div class="col-md-7 text-center" id="left-side">
        <div class="row d-flex justify-content-center align-items-center min-vh-100">
            <img src="{{ asset('img/ub1.png') }}" alt="" width="350px" height="400px" style="padding:0; margin: 0;">
        </div>
      </div>
      
      <div class="col-md-5 min-vh-100 d-flex justify-content-center align-items-center " id="right-side">
        <div class="col-md-12 min-vh-100 d-flex align-items-center">
          <div class="row">

            <div class="col-md-12 text-center mb-2" >
              <h4><strong class="roboto-bold">DOCUMENT MANAGEMENT SYSTEM</strong></h4>
              <div class="card-subtitle roboto-medium" >
                <span style="color: #94a3b8; font-size:12px;">
                  <b>Find what you need, fast.</b> Streamline your workflow with easy access to all your documents.
                </span>
              </div>
            </div>

            <div class="col-md-12 " id="right-side">
              <div class="d-flex align-items-center justify-content-center">
                @if (session()->has('success'))
                  <div class="col-md-8 alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                  </div>
                @elseif (session()->has('error'))  
                  <div class="col-md-8 alert alert-danger alert-dismissible fade show" role="alert">
                      {{ session('error') }}
                  </div>
                @else
                @endif
              </div>
              
              <form action="{{ route('login') }}" method="post" class="m-2">
                @csrf
                <div class="row d-flex align-items-center justify-content-center">
                
                  <div class="mb-3 col-md-8">
                      <input type="text" name="username" id="username" placeholder="Username" class="form-control rounded-pill text-center shadow " required>
                  </div>

                  <div class="mb-3 col-md-8">
                      <input type="password" name="password" id="password" placeholder="Password" class="form-control rounded-pill text-center shadow " required>
                  </div>
                  <div class="mb-3 d-flex align-items-center justify-content-end col-md-8">
                      <button type="submit" class="btn btn-success rounded-pill p-2 m-0" style="width: 120px;">Login</button>
                  </div>
                </div>
              </form>
            </div>
          
            <div style="position: absolute; bottom:0; left:0; right:0; text-align:center; " >
              <p style="color: #94a3b8; font-size:12px;">
                Developed by the <a href="" style="text-decoration: none; color:#6ee7b7;">University Systems Development Office</a> © 2024
              </p>
            </div>
          </div>
        </div>

        
      </div>
    </div>
  </div>

  

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js')}}"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="{{ asset('js/mdb.js') }}"></script>
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
  <!-- Bootstrap tooltips -->



  <!-- Custom scripts -->
  <script>

    new WOW().init();

  </script>

</body>

</html>
