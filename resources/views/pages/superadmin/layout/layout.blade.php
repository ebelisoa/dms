<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', 'DMS-Superadmin')</title>

    <link rel="icon" type="image/x-icon" href="{{ asset('img/dmmmsu_logo.png') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/mdb.min.css')}}">

</head>
<body class="fixed-sn white-skin">
    @include('pages.superadmin.inc.header')
    <main>
        @yield('content')
    </main>
    
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="{{ asset('js/jquery-3.4.1.min.js')}}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/mdb.js') }}s"></script>
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <!-- Initializations -->

    @yield('script')
</body>
</html>