@extends('pages.superadmin.layout.layout')
@section('title', 'DMS-Manage Category')
@section('content')
    <div class="container-fluid">
        <div class="row">
            
            <div class="card container-fluid">
                <div class="card-header" style="background-color: transparent;">
                    <div class="float-end">
                        <a href="{{ route('superadmin.create.documenttype') }}">
                            <button class="btn btn-success rounded-pill p-1" style="width: 200px;">Create Document Type</button>
                        </a>
                    </div>
                </div>
                <div class="mt-5">
                    @if (session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ session('success') }}</strong>
                        </div>
                        @elseif (session()->has('error'))
                        <div class="alert alert-error alert-dismissible fade show" role="alert">
                            <strong>{{ session('Error') }}</strong>
                        </div>
                    @else
                    
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Document Type Name</th>
                                <th class="text-center">Action</th>
                            </thead>
                            <tbody>
                            
                                @foreach ($documenttype AS $dt )
                                
                                    <tr>
                                        <td>{{ $dt->document_type_name }}</td>
                                        <td class="text-center">
                                            <div class="row text-center">
                                                <a href="{{ route('superadmin.edit.documenttype', ['id'=>$dt]) }}">
                                                    <button class="btn btn-warning rounded-pill p-2">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width: 25px; height:25px;">
                                                            <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                                                        </svg>
                                                    </button>
                                                </a>
                                                <a href="">
                                                    <button class="btn btn-danger rounded-pill p-2">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width: 25px; height:25px;">
                                                            <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                                        </svg>
                                                    </button>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <thead>
                                    <th>Document Type Name</th>
                                    <th class="text-center">Action</th>
                                </thead>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    
@endsection