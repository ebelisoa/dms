@extends('pages.superadmin.layout.layout')
@section('title')
@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header" style="background-color:transparent;">
               
                <div class="float-right">
                    <a href="{{ route('superadmin.create.users') }}">
                      <button class="btn btn-success rounded-pill p-1" style="width: 120px; ">Create</button>
                    </a>
                </div>
                <h2 class="text-center">USER MANAGEMENT</h2>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Fullname</th>
                            <th>Username</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $d)
                            @if (strtoupper($d->username) == 'SUPERADMIN')
                            @else
                                <tr id="{{ $d->id }}">
                                    <td>{{ $d->fullname }}</td>
                                    <td>{{ $d->username }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('superadmin.edit.users',['id'=>$d->id]) }}">
                                            <button class="btn btn-warning p-1 m-1 rounded-pill" style="width: 120px;">Update</button>
                                        </a>
                                        <a href="" id="delete">
                                            <button class="btn btn-danger p-1 m-1 rounded-pill" style="width: 120px;">
                                                Delete
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endif
                            
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    {{-- <script>
        $(document).ready(function(){

        });
    </script> --}}
@endsection