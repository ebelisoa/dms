@extends('pages.superadmin.layout.layout')
@section('title')
@section('content')
    <div class="container-fluid">
        <div class="card col-md-6">
            <div class="card-header" style="background-color: transparent;">
                <div class="float-end">
                    <a href="{{ route('superadmin.index.category') }}">
                        <button class="btn btn-success rounded-pill" style="width: 120px;">Back</button>
                    </a>
                </div>
            </div>
            
            <div class="card-body text-center">
                <h2 class="card-title">Create Category</h2>
            </div>
            <div>
                @if (session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ session('success') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @elseif (session()->has('error'))
                    <div class="alert alert-error alert-dismissible fade show" role="alert">
                        <strong>{{ session('Error') }}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @else
                
                @endif
            </div>

            <div class="card-body">
                <form action="{{ route('superadmin.store.category') }}" method="post">
                     @csrf
                    <div class="row mb-3">
                        <label for="" class="col-md-3 col-form-label">Category Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="categoryName" id="categoryName" required>
                        </div>
                    </div>
                    <div class="row mb-3 ">
                        <div class="row d-flex justify-content-center">
                            <button type="submit" class="btn btn-success rounded-pill" style="width: 120px;">Create</button>
                            <button type="reset" class="btn btn-danger rounded-pill" style="width: 120px;">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    
@endsection