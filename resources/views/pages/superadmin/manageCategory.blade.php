@extends('pages.superadmin.layout.layout')
@section('title', 'DMS-Manage Category')
@section('content')
    <div class="container-fluid">
        <div class="row">
            
            <div class="card container-fluid">
                <div class="card-header" style="background-color: transparent;">
                    <div class="float-end">
                        <a href="{{ route('superadmin.create.category') }}">
                            <button class="btn btn-success rounded-pill" style="width: 200px;">Create Category</button>
                        </a>
                    </div>
                </div>
                <div class="mt-5">
                    @if (session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ session('success') }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @elseif (session()->has('error'))
                        <div class="alert alert-error alert-dismissible fade show" role="alert">
                            <strong>{{ session('Error') }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @else
                    
                    @endif
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Category Name</th>
                                <th class="text-center">Action</th>
                            </thead>
                            <tbody>
                                @foreach ($category as $cat )
                                    <tr>
                                        <td>{{ $cat->categoryName; }}</td>
                                        <td class="text-center">
                                            <div class="row">
                                                <div class="col">
                                                    <a href="">
                                                        Update
                                                    </a>
                                                </div>
                                                <div class="col">
                                                    <a href="">
                                                        Delete
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    
@endsection