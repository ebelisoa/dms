@extends('pages.admin.layout.layout')
@section('title', 'DMS-Dashboard')
@section('content')
<div class="container-fluid">
  <!-- Section: Main panel -->
 
  <div class="row">
    <div class="col-md-3"></div>

  
    <div class="col-md-6 ">
      <!-- Card -->
        <div class="card">
            <div class="card-header" style="background-color: transparent; text-align:center;">
                <div class="row">
                  <div class="col-md-4">
                    <img src="{{ asset('img/Bagong_Pilipinas_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                    <img src="{{ asset('img/dmmmsu_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                  </div>
                  <div class="col-md-8 align-middle" style="text-align: left;">
                    <h2><strong>Update User Account</strong></h2>
                  </div>
                </div>
            </div>
        </div>


      <div class="card card-cascade narrower d-flex content-justify-center">
        
        
        <div class="card-body">
          <div class="mt-2">
            @if (session()->has('success'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                  {{ session('success') }}
              </div>
            @elseif (session()->has('error'))  
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {{ session('error') }}
              </div>
            @else
              
            @endif
          </div>
          

          <form action="{{ route('admin.update.users', ['id' => $userInformation ]) }}" method="post" enctype="multipart/form-data" >
            @method('put')
            @csrf
           
            <div class="row mb-3">
              <label for="" class="col-md-3 col-form-label">Username:</label>
              <div class="col-md-9">
                <input type="text" name="username" id="username" class="form-control" value="{{ $userInformation->username }}">
              </div>
            </div>

            <div class="row mb-3">
              <label for="" class="col-md-3 col-form-label">Old Password:</label>
              <div class="col-md-9">
                <input type="password" name="old_password" id="old_password" class="form-control">
              </div>
            </div>
            
            <div class="row mb-3">
              <label for="" class="col-md-3 col-form-label">New Password:</label>
              <div class="col-md-9">
                <input type="password" name="new_password" id="new_password" class="form-control">
              </div>
            </div>
            
            
            <div class="row mb-3 ">
              <div class="col-md-12 justify-content-center ">
                <div class="row justify-content-center">
                    <button type="submit" class="btn btn-success rounded-pill" style="width: 120px;" id="submit">Update</button>
                    <button type="reset" class="btn btn-danger rounded-pill" style="width: 120px;">Reset</button>
                </div>
              </div>

            </div>

          </form>
        </div>
      </div>
      <!-- Card -->
    </div>
    <div class="col-md-3"></div>
  </div>

</div>

@endsection
@section('script')
  <script>
    // SideNav Initialization
  $(".button-collapse").sideNav();

let container = document.querySelector('.custom-scrollbar');
var ps = new PerfectScrollbar(container, {
  wheelSpeed: 2,
  wheelPropagation: true,
  minScrollbarLength: 20
});
    
  </script>
@endsection

  


