@extends('pages.superadmin.layout.layout')
@section('title')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header" style="background-color:transparent;">
                        <h2 class="text-center" >
                            Create Users
                        </h2>
                      
                    </div>
                    <div class="card-title" style="background-color:transparent;">
                        <div class="float-right">
                            <a href="{{ route('superadmin.index.users') }}">
                                <button class="btn btn-success rounded-pill m-1 p-1" style="width: 120px;">Back</button>
                            </a>
                        </div>
                       {{--  <h2 class="text-center" >
                            Create Users
                        </h2> --}}
                      
                    </div>
                    <div class="card-body">
                        <div class="mt-2">
                            @if (session()->has('success'))
                              <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  {{ session('success') }}
                              </div>
                            @elseif (session()->has('error'))  
                              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                  {{ session('error') }}
                              </div>
                            @else
                              
                            @endif
                        </div>
                    
                        <form action="{{ route('superadmin.store.users') }}" method="POST" >
                            @csrf
                            <div class="row mb-3">
                                <label for="" class="col-md-3 col-form-label">Fullname</label>
                                <div class="col-md-9">
                                    <input type="text" name="fullname" id="fullname" class="form-control">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="" class="col-md-3 col-form-label">Username</label>
                                <div class="col-md-9">
                                    <input type="text" name="username" id="fullname" class="form-control">
                                </div>
                            </div>  
                            <div class="row mb-3">
                                <label for="" class="col-md-3 col-form-label">Password</label>
                                <div class="col-md-9">
                                    <input type="password" id="password" name="password" class="form-control">
                                </div>
                            </div>
                            <div class="row mb-3 ">
                                <div class="col-md-12 justify-content-center ">
                                  <div class="row justify-content-center">
                                      <button type="submit" class="btn btn-success rounded-pill" style="width: 120px;" id="submit">Submit</button>
                                      <button type="reset" class="btn btn-danger rounded-pill" style="width: 120px;">Reset</button>
                                  </div>
                                </div>
                              </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
@endsection
@section('script')
    
@endsection