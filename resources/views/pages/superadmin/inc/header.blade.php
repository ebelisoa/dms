
  <!-- Main Navigation -->
  <header>

    <!-- Sidebar navigation -->
    <div id="slide-out" class="side-nav sn-bg-4 fixed">
      <ul class="custom-scrollbar">

        <!-- Logo -->
        <li class="logo-sn waves-effect py-3">
          <div class="text-center">
            <h1 style="color:black; font-weight:bold;">DMS</h1>
          </div>
        </li>
        <hr>

        <!-- Side navigation links -->
        <li>
          <ul class="collapsible collapsible-accordion">
            <!-- Simple link -->
            
            <li>
              <a href="{{ route('superadmin.index.category') }}" class="collapsible-header waves-effect">Category</a>
              <a href="{{ route('superadmin.index.documenttype') }}" class="collapsible-header waves-effect">Document Type</a>
              <a href="{{ route('superadmin.index.users') }}" class="collapsible-header waves-effect">User Management</a>
              <a href="" class="collapsible-header waves-effect">My Accounts</a>
              <a href="{{ route('logout') }}" class="collapsible-header waves-effect">Logout</a>
            </li>

          </ul>
        </li>
        <!-- Side navigation links -->

      </ul>
      <div class="sidenav-bg mask-strong"></div>
    </div>
    <!-- Sidebar navigation -->

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg scrolling-navbar double-nav">

      <!-- SideNav slide-out button -->
      <div class="float-left">
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i></a>
      </div>

      <!-- Breadcrumb -->
      <div class="breadcrumb-dn mr-auto">
        <p>Document Management System</p>
      </div>

      <div class="d-flex change-mode">
        <!-- Navbar links -->
        <ul class="nav navbar-nav nav-flex-icons ml-auto">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-user"></i> <span class="clearfix d-none d-sm-inline-block">Profile</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="{{-- {{ route('logout') }} --}}">Log Out</a>
              <a class="dropdown-item" href="#">My account</a>
            </div>
          </li>

        </ul>
        <!-- Navbar links -->

      </div>

    </nav>
    <!-- Navbar -->
  </header>
  <!-- Main Navigation -->