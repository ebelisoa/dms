@extends('pages.admin.layout.layout')
@section('title', 'DMS-Dashboard')
@section('content')
    <div class="container-fluid mb-5">
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2 text-center align-items-center justify-content-center">
                        <img src="{{ asset('img/Bagong_Pilipinas_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                    </div>
                    <div class="col-md-8 text-center d-flex align-items-center justify-content-center">
                        <h2 class="text-center"><strong>REPORTS</strong></h2>
                    </div>
                    <div class="col-md-2 text-center align-items-center justify-content-center">
                        <img src="{{ asset('img/dmmmsu_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <form action="{{ route('admin.print_pdf.reports') }}"  method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-3 p-1">
                                <select name="category" id="category" class="form-control">
                                    <option value="" disabled selected>Choose Category</option>
                                 
                                    @foreach ($category as $categories)
                                        <option value="{{ $categories->id }}">{{ $categories->categoryName }}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="col-md-3 p-1">
                                <select name="year" id="year" class="form-control" >
                                    <option value="" disabled selected>Choose Year</option>
                                  
                                    @foreach ($years as $year )
                                        <option value="{{ $year }}">{{ $year }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-3 p-1">
                                <select name="month" id="month" class="form-control" >
                                    <option value="" disabled selected>Choose Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <x-button-primary id="submit">Generate</x-button-primary>
                                    <x-button-danger type="submit">Print</x-button-danger>
                                </div>
                            </div>
                           
                        </div>
                      
                    </form>    
                  
                </div>
                
                <table class="table">
                    <thead>
                        <tr>
                            <th>Meeting Category</th>
                            <th class="text-center">Total Meeting</th>
                           {{--  <th class="text-center">Details</th> --}}
                        </tr>
                    </thead>
                    <tbody id="tbody">
                        @foreach ($category as $cat )
                            <tr>
                                <td>{{ $cat->categoryName }}</td>
                                @if($cat->categoryName == 'Academic Council')
                                    <td class="text-center">{{ $adt }}</td>
                                @elseif ($cat->categoryName == 'Admin Council')
                                    <td class="text-center">{{ $act }}</td>
                                @elseif ($cat->categoryName == 'Preboard Meeting')
                                    <td class="text-center">{{ $pbmm }}</td>
                                @elseif ($cat->categoryName == 'Regular Board Meeting')
                                    <td class="text-center">{{ $rbmm }}</td>
                                @elseif ($cat->categoryName == 'Special Meeting')
                                    <td class="text-center">{{ $spm }}</td>
                                @else
                                
                                @endif
                                {{-- <td class="text-center">
                                    <a href="{{ route('admin.reports.viewReports', ['id'=>$cat->id]) }}">
                                        <button class="btn btn-success p-1 m-1" style="width:150px;">View Details</button>
                                    </a>
                                </td> --}}
                            </tr>
                        @endforeach
                          
                    </tbody>
               </table>
            </div>
      
        </div>
    </div>
@endsection

@section('script')
  <script>
        $(document).ready(function(){
            
            $('#submit').on('click', function(e){
                e.preventDefault();
                var category = $('#category').val();
                var year = $('#year').val();
                var month = $('#month').val();
           
                if(category == null ){
                    window.alert('Please Select a Category');
                }else{   
                    $.ajax({
                        url:"{{route('admin.filter_year.reports')}}",
                        type:"GET",
                        data:{ 
                            category:category,
                            year:year,
                            month:month,
                        },
                        success:function(res){
                            console.log(res);

                            var action = res.action;
                            var admin = res.admin;
                            var academic = res.academic;
                            var preboard = res.preboard;
                            var regularboard = res.regularboard;
                            var specialmeeting = res.specialmeeting;
                            var total = res.total;
                        

                            var html = '';
                            if(total.length > 0){
                                for(let i = 0; i<total.length;i++){
                                    html +='<tr>\
                                        <td>'+total[i]['category']+'</td>\
                                        <td class="text-center">'+total[i]['count']+'</td>\
                                        </tr>';
                                }
                            }
                            $('#tbody').html(html);
                        }
                    });
                }
            });

          
        });
  </script>
@endsection

    


  

  


