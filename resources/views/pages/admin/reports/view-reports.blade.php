@extends('pages.admin.layout.layout')
@section('title', 'DMS-Dashboard')
@section('content')
    <div class="container-fluid">

        <div class="card">
            <div class="card-header" style="background-color: transparent;">
                <h2 class="text-center"><strong>{{ strtoupper($category_name) }} - REPORTS</strong></h2>
            </div>
            <div class="card-body">
                <form action="" class="mb-3">
                    @csrf
                    <div class="row">
                        <div class="col-md-3">
                            <select name="year" id="year" class="form-control" >
                              
                                <option value="" disabled selected>Choose Year</option>
                                <option value="0">All</option>
                                @foreach ($years as $y )
                                <option value="{{ $y }}">{{ $y }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="month" id="month" class="form-control">
                                <option value="" disabled selected>Choose Month</option>
                                <option value="00">All</option>
                                <option value="01" >January</option>
                                <option value="02" >February</option>
                                <option value="03" >March</option>
                                <option value="04" >April</option>
                                <option value="05" >May</option>
                                <option value="06" >June</option>
                                <option value="07" >July</option>
                                <option value="08" >August</option>
                                <option value="09" >September</option>
                                <option value="10" >October</option>
                                <option value="11" >November</option>
                                <option value="12" >December</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success rounded-pill p-2 m-0" style="width: 200px;" id="generate">Generate</button>
                        </div>
                    </div>
                </form>
                <div id="table">
                    
                </div>

                <table>
                    <thead>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                               <td>{{ $item->meeting_date }}</td>
                              
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>
@endsection

@section('script')
  <script>
        $('document').ready(function(){

            $('#generate').on('click', function(e){
                e.preventDefault();
                var year = $('#year').val();
                var month = $('#month').val();
              
                if(year == null && month == null || year == null || month == null ){
                    alert('Please Fill the data ');
                }else{
                    $.ajax({
                        url:'{{ route("admin.reports.fetchData") }}',
                        type:'POST',
                        data: {
                            _token: '{{ csrf_token() }}',
                            year:year,
                            month:month,
                        },
                        dataType: 'JSON',
                        success: function(data){
                            console.log(data);
                        }
                    });
                }
            });

        });
  </script>
@endsection

    


  

  


