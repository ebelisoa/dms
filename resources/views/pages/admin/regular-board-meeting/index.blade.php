@extends('pages.admin.layout.layout')
@section('title', 'DMS-Admin Council')
@section('content')

<div class="container-fluid mb-5">

  {{-- First Row --}}
  <div class="card mb-3">
    <div class="card-body">
      <div class="row">
        <div class="col-md-2 text-center align-items-center justify-content-center" >
            <img src="{{ asset('img/Bagong_Pilipinas_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
        </div>
        <div class="col-md-8 text-center align-items-center justify-content-center" >
          <h2 class="my-4 dark-grey-text font-weight-bold card-title shadow-5">REGULAR BOARD MEETING</h2>
        </div>
        <div class="col-md-2 text-center align-items-center justify-content-center">
          <img src="{{ asset('img/dmmmsu_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
        </div>
      </div>
    </div>
  </div>
      
     
      <div class="card" style="overflow: auto;">
        
        <div class="card-header" style="background-color:transparent;">
          <div class="d-flex align-items-center justify-content-end">
            <a href="{{ route('admin.regular-board-meeting.create', ['id' => $category ]) }}">
              <x-button-primary>Create</x-button-primary>
            </a>
          </div>
        </div>

        <div class="card-body">
          <div class="mt-2">
            @if (session()->has('success'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                  {{ session('success') }}
                 
              </div>
            @elseif (session()->has('error'))  
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {{ session('error') }}
                  
            @else
              
            @endif
          </div>
          <table id="dtMaterialDesignExample" class="table table-striped" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th  class="text-center">Meeting Date</th>
                <th  class="text-center">Meeting Time</th>
                <th  class="text-center">Document Type</th>
                <th  class="text-center">Document Description</th>
                <th  class="text-center">Quarter</th>
                <th  class="text-center">Remarks</th>
                
                <th class="text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width: 25px; height:25px;">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 13.5l10.5-11.25L12 10.5h8.25L9.75 21.75 12 13.5H3.75z" />
                  </svg>                  
                </th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($meeting_details as $md )
                <tr>
                  <td class="text-center">{{ date('F j, Y', strtotime($md->meeting_date )) }}</td>
                  <td class="text-center">{{ $md->meeting_time }}</td>
                  <td class="text-center">{{ strtoupper($md->document_type) }}</td>
                  <td class="text-center"> {{-- Document Description --}}
                    <a href="" class="btn btn-success btn-rounded p-1" style="width: 100px;" data-toggle="modal" data-target="#document{{ $md->id }}" >View</a>
                    <div class="modal fade" id="document{{ $md->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-notify modal-info" role="document">
                      <div class="modal-content">
                        <div class="modal-header" style="background-color: #16a34a; ">
                          <p class="heading lead text-center">Document Description</p>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="white-text">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <p>
                            {{ $md->document_description }}
                          </p>
                        </div>
                        <div class="modal-footer justify-content-center">
                          <a type="button" class="btn btn-outline-success waves-effect" data-dismiss="modal">Close</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  </td>
                  <td  class="text-center">
                      @if (empty($md->quarter))
                          No Data Available
                      @else
                        @if ($md->quarter == 1)
                          First
                        @elseif ($md->quarter == 2)
                          Second
                        @elseif($md->quarter == 3)
                          Third
                        @elseif($md->quarter == 4)
                          Fourth
                        @endif
                      @endif
                  </td>
                  <td  class="text-center">
                    <a href="" class="btn btn-success btn-rounded p-1" style="width: 100px;" data-toggle="modal" data-target="#remarks{{ $md->id }}" >View</a>
                    <div class="modal fade" id="remarks{{ $md->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-notify modal-info" role="document">
                        <div class="modal-content">
                      
                          <div class="modal-header" style="background-color: #16a34a; ">
                            <p class="heading lead text-center">Remarks/Actions</p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true" class="white-text">&times;</span>
                            </button>
                          </div>

                    
                          <div class="modal-body">
                            <p>
                              {{ $md->remarks }}
                            </p>
                          </div>

                        
                          <div class="modal-footer justify-content-center">
                            <a type="button" class="btn btn-outline-success waves-effect" data-dismiss="modal">Close</a>
                          </div>
                        </div>
                      
                      </div>
                    </div>
                  </td>

                
                  <td  class="text-center">
                    <a href="{{ route('admin.regular-board-meeting.edit',['meeting_details' => $md ]) }}" class="btn btn-success rounded-pill p-2">
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width: 20px; heigth:20px;">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                      </svg>       
                      
                    </a>
                      
                    <a class="btn btn-danger btn-rounded p-2" data-toggle="modal" data-target="#delete{{ $md->id }}">
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width: 20px; heigth:20px;">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                      </svg>
                    </a>
                    <!-- Central Modal Info Demo -->
                    <div class="modal fade" id="delete{{ $md->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-notify modal-info" role="document">
                      <!-- Content -->
                      <div class="modal-content">
                        <!-- Header -->
                        <div class="modal-header" style="background-color: #16a34a; ">
                          <p class="heading lead text-center">Delete Meeting Details</p>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="white-text">&times;</span>
                          </button>
                        </div>

                        <div class="modal-body">
                          Do you really want to delete these records? This process cannot be undone.
                          <form action="{{ route('admin.regular-board-meeting.destroy', ['id' => $md]) }}" method="post">
                            @method('delete')
                            @csrf
                        </div>

                        <div class="modal-footer justify-content-center">
                          <button type="submit" class="btn btn-outline-danger waves-effect">Delete</button>
                          <button type="button" class="btn btn-outline-success waves-effect" data-dismiss="modal">Close</button>
                        </div>
                          </form>
                      </div>
                      <!-- Content -->
                    </div>
                  </div>

                  </td>
                  <td>
                    <a href="{{ route('admin.files.index', ['meeting_details' => $md ]) }}" class="btn btn-success btn-rounded p-2" data-mdb-tooltip-init title="View Files">
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width: 20px; heigth:20px;">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 12.75V12A2.25 2.25 0 0 1 4.5 9.75h15A2.25 2.25 0 0 1 21.75 12v.75m-8.69-6.44-2.12-2.12a1.5 1.5 0 0 0-1.061-.44H4.5A2.25 2.25 0 0 0 2.25 6v12a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9a2.25 2.25 0 0 0-2.25-2.25h-5.379a1.5 1.5 0 0 1-1.06-.44Z" />
                      </svg>                          
                    </a>
                  </td>
                </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th class="text-center">Meeting Date</th>
                <th class="text-center">Meeting Time</th>
                <th class="text-center">Document Type</th>
                <th class="text-center">Document Description</th>
                <th class="text-center">Quarter</th>
                <th class="text-center">Remarks</th>
              
                <th class="text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width: 25px; height:25px;">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 13.5l10.5-11.25L12 10.5h8.25L9.75 21.75 12 13.5H3.75z" />
                  </svg>                  
                </th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>

    
 
</div>

@endsection
@section('script')
<script>
  // SideNav Initialization
  $(".button-collapse").sideNav();

  let container = document.querySelector('.custom-scrollbar');
  var ps = new PerfectScrollbar(container, {
    wheelSpeed: 2,
    wheelPropagation: true,
    minScrollbarLength: 20
  });

  $('#dtMaterialDesignExample').DataTable();

  $('#dt-material-checkbox').dataTable({

    columnDefs: [{
      orderable: false,
      className: 'select-checkbox',
      targets: 0
    }],
    select: {
      style: 'os',
      selector: 'td:first-child'
    }
  });

  $('#dtMaterialDesignExample_wrapper, #dt-material-checkbox_wrapper').find('label').each(function () {
    $(this).parent().append($(this).children());
  });
  $('#dtMaterialDesignExample_wrapper .dataTables_filter, #dt-material-checkbox_wrapper .dataTables_filter').find(
    'input').each(function () {
    $('input').attr("placeholder", "Search");
    $('input').removeClass('form-control-sm');
  });
  $('#dtMaterialDesignExample_wrapper .dataTables_length, #dt-material-checkbox_wrapper .dataTables_length').addClass(
    'd-flex flex-row');
  $('#dtMaterialDesignExample_wrapper .dataTables_filter, #dt-material-checkbox_wrapper .dataTables_filter').addClass(
    'md-form');
  $('#dtMaterialDesignExample_wrapper select, #dt-material-checkbox_wrapper select').removeClass(
    'custom-select custom-select-sm form-control form-control-sm');
  $('#dtMaterialDesignExample_wrapper select, #dt-material-checkbox_wrapper select').addClass('mdb-select');
  $('#dtMaterialDesignExample_wrapper .mdb-select, #dt-material-checkbox_wrapper .mdb-select').materialSelect();
  $('#dtMaterialDesignExample_wrapper .dataTables_filte, #dt-material-checkbox_wrapper .dataTables_filterr').find(
    'label').remove();

</script>
@endsection
