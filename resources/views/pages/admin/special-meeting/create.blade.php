@extends('pages.admin.layout.layout')
@section('title', 'DMS-Dashboard')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="card mb-3">
          <div class="card-header" style="background-color: transparent; text-align:center;">
              <div class="row">
                <div class="col-md-2 text-center align-items-center justify-content-center" >
                    <img src="{{ asset('img/Bagong_Pilipinas_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                  </div>
                  <div class="col-md-8 text-center align-items-center justify-content-center" >
                    <h2 class="my-4 dark-grey-text font-weight-bold card-title shadow-5">SPECIAL MEETING FORM</h2>
                  </div>
                  <div class="col-md-2 text-center align-items-center justify-content-center">
                    <img src="{{ asset('img/dmmmsu_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                  </div>
              </div>
          </div>
        </div>

        <div class="card ">
            <div class="card card-header" style="background-color: transparent;">
                <div class="d-flex justify-content-end align-items-center">
                    <a href="{{ route('admin.special-meeting.index') }}">
                        <x-button-primary>Back</x-button-primary>
                    </a>
                </div>
            </div>
        
            <div class="card-body">
                <div class="mt-2">
                    @if (session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('success') }}
                    </div>
                    @elseif (session()->has('error'))  
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error') }}
                    </div>
                    @else
                    
                    @endif
                </div>
                <form action="{{ route('admin.special-meeting.store') }}" method="post" enctype="multipart/form-data" >
                    @csrf
                    <input type="hidden" name="category_id" id="category_id" value="{{ $category }}">
                    <div class="row mb-3">
                        <label for="" class="col-md-3 col-form-label">Meeting Date And Time</label>
                        <div class="col-md-9">
                            <input type="date" name="meeting_date" id="meeting_date" class="form-control"><br>
                            <input type="time" name="meeting_time" id="meeting_time" class="form-control">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="" class="col-md-3 col-form-label">Document Type:</label>
                        <div class="col-md-9">
                            <select name="document_type" id="document_type" class="form-control">
                                <option value="">-- Please Select -- </option>
                                <option value="agenda">Agenda</option>
                                <option value="resolution">Resolution</option>
                                <option value="minutes">Minutes</option>
                                <option value="attendance">Attendance</option>
                                <option value="memo">Memo</option>
                                <option value="documentation">Documentation</option>
                            </select>
                        </div>
                    </div>
            
            <div class="row mb-3">
              <label for="" class="col-md-3 col-form-label">Document Description:</label>
              <div class="col-md-9">
                <textarea cols="30" rows="5" name="document_description" id="document_description" class="form-control"></textarea>
              </div>
            </div>

            <div class="row mb-3" id="agenda_form">
              <label for="" class="col-md-3 col-form-label">Agenda:
                <a id="add_agenda_upload" class="btn btn-success rounded-pill p-1">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width:25px; height:25px;">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                  </svg>
                </a>
              </label>
              <div class="col-md-9" id="agenda_container">
                  <input type="file" name="agenda[]" id="agenda[]" class="form-control mb-3 p-1">
              </div>
            </div>


            <div class="mb-3" id="resolution">
              <label for="" class="col-md-3 col-form-label"><strong>Resolution</strong></label>

                <div class="row mb-3">
                  <label for="" class="col-md-2 col-form-label">No:</label>
                  <div class="col-md-9">
                    <input type="number" name="resolution_number" id="resolution_number" class="form-control">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="" class="col-md-2 col-form-label ">Subject:</label>
                  <div class="col-md-9">
                    <input type="text" name="resolution_subject" id="resolution_subject" class="form-control">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="" class="col-md-2 col-form-label ">Add File:
                    <a id="add_resolution_upload" class="btn btn-success rounded-pill p-1">
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width:25px; height:25px;">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                      </svg>
                    </a>
                  </label>
                  <div class="col-md-9" id="resolution_container">
                    <input type="file" name="resolution_file[]" id="resolution_file[]" class="form-control mb-3 p-1" >
                  </div>
                </div>
            </div>

            <div class="row mb-3" id="minutes_tab">
              <label for="" class="col-md-3 col-form-label">Minutes:  
                <a id="add_minutes_upload" class="btn btn-success rounded-pill p-1">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width:25px; height:25px;">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                  </svg>
                </a>
            </label>
              <div class="col-md-9" id="minutes_container">
                <input type="file" name="minutes_file[]" id="minutes_file[]"  class="form-control mb-3 p-1" >
              </div>
            </div>

            <div class="row mb-3" id="attendance_tab">
              <label for="" class="col-md-3 col-form-label">Attendance:
                <a id="add_attendance_upload" class="btn btn-success rounded-pill p-1">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width:25px; height:25px;">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                  </svg>
                </a>
              </label>
              <div class="col-md-9" id='attendance_container'>
                <input type="file" name="attendance_file[]" id="attendance_file[]" class="form-control mb-3 p-1" >
              </div>
            </div>

            <div class="mb-3" id="memo_tab">
              <label for="" class="col-md-3 col-form-label"><strong>Memo</strong></label>
                <div class="row mb-3">
                  <label for="" class="col-md-3 col-form-label">No:</label>
                  <div class="col-md-9">
                    <input type="number" name="memo_number" id="memo_number" class="form-control" >
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="" class="col-md-3 col-form-label">Subject:</label>
                  <div class="col-md-9">
                    <input type="text" name="memo_subject" id="memo_subject" class="form-control" >
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="" class="col-md-3 col-form-label">File:
                    <a id="add_memo_upload" class="btn btn-success rounded-pill p-1">
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width:25px; height:25px;">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                      </svg>
                    </a>
                  </label>
                  <div class="col-md-9" id="memo_container">
                    <input type="file" id="memo_file[]" name="memo_file[]" class="form-control mb-3 p-1" >
                  </div>
                </div>
            </div>

            <div class="mb-3" id="documentation_tab">
              <label for="" class="col-md-3 col-form-label"><strong>Documentation</strong></label>
              <div class="row mb-3">
                <label for="" class="col-md-3 col-form-label">Files:</label>
                <div class="col-md-9">
                  <input type="file" id="image_file[]" name="image_file[]" class="form-control mb-3 p-1" multiple>
                </div>
              </div>

              <div class="row mb-3">
                <label for="" class="col-md-3 col-form-label">Drive Link (Optional):</label>
                <div class="col-md-9">
                  <input type="text" name="link" id="link" class="form-control">
                </div>
              </div>
              
            </div>

              <div class="row mb-3">
                <label for="" class="col-md-3 col-form-label">Quarter</label>
                <div class="col-md-9">
                  <select name="quarter" id="quarter" class="form-control" >
                    <option value="">--Please Select Quarter--</option>
                    <option value="1">First Quarter</option>
                    <option value="2">Second Quarter</option>
                    <option value="3">Third Quarter</option>
                    <option value="4">Fourth Quarter</option>
                  </select>
                </div>
              </div>

            <div class="row mb-3">
              <label for="" class="col-md-3 col-form-label">Remarks/Action:</label>
              <div class="col-md-9">
                <textarea name="remarks" id="remarks" cols="30" rows="5" class="form-control"></textarea>
              </div>
            </div>

            <div class="row mb-3 ">
              <div class="col-md-12 justify-content-center ">
                <div class="row justify-content-center">
                    <x-button-primary type="submit">Submit</x-button-primary>
                    <x-button-danger type="reset">Reset</x-button-danger>
                </div>
              </div>

            </div>

          </form>
        </div>
      </div>
      <!-- Card -->
    </div>
    <div class="col-md-3"></div>
  </div>

</div>

@endsection
@section('script')
  <script>
    // SideNav Initialization
    $(".button-collapse").sideNav();

    let container = document.querySelector('.custom-scrollbar');
    var ps = new PerfectScrollbar(container, {
      wheelSpeed: 2,
      wheelPropagation: true,
      minScrollbarLength: 20
    });
    
     
    




      $(document).ready(function(){

          $('#agenda_form').hide();
          $('#resolution').hide();
          $('#minutes_tab').hide();
          $('#attendance_tab').hide();
          $('#memo_tab').hide();
          $('#documentation_tab').hide();

          document.getElementById('document_type').addEventListener('change', function(){
            var doc_type = document.getElementById('document_type').value
            if(doc_type == 'agenda'){
              $('#resolution').hide();
              $('#minutes_tab').hide();
              $('#attendance_tab').hide();
              $('#memo_tab').hide();
              $('#documentation_tab').hide();
              $('#agenda_form').show();
            }else if(doc_type == 'resolution'){
              $('#agenda_form').hide();
              $('#minutes_tab').hide();
              $('#attendance_tab').hide();
              $('#memo_tab').hide();
              $('#documentation_tab').hide();
              $('#resolution').show();
            }else if(doc_type == 'minutes'){
              $('#agenda_form').hide();
              $('#resolution').hide();
              $('#attendance_tab').hide();
              $('#memo_tab').hide();
              $('#documentation_tab').hide();
              $('#minutes_tab').show();
            }else if(doc_type == 'attendance'){
              $('#agenda_form').hide();
              $('#resolution').hide();
              $('#minutes_tab').hide();
              $('#memo_tab').hide();
              $('#documentation_tab').hide();
              $('#attendance_tab').show();
            }else if(doc_type == 'memo'){
              $('#agenda_form').hide();
              $('#resolution').hide();
              $('#minutes_tab').hide();
              $('#attendance_tab').hide();
              $('#documentation_tab').hide();
              $('#memo_tab').show();
            }else if(doc_type == 'documentation'){
              $('#agenda_form').hide();
              $('#resolution').hide();
              $('#minutes_tab').hide();
              $('#attendance_tab').hide();
              $('#memo_tab').hide();
              $('#documentation_tab').show();
            }
            
          });
      });

      const agenda_container = document.getElementById('agenda_container')
      document.getElementById('add_agenda_upload').addEventListener('click', function(){
          const child = document.createElement('input')
          child.setAttribute('type', 'file')
          child.setAttribute('name', 'agenda[]')
          child.setAttribute('class', 'form-control mb-3 p-1')
          agenda_container.appendChild(child)
      });

      const resolution_container = document.getElementById('resolution_container')
      document.getElementById('add_resolution_upload').addEventListener('click', function(){
          const child = document.createElement('input')
          child.setAttribute('type', 'file')
          child.setAttribute('name', 'resolution_file[]')
          child.setAttribute('class', 'form-control mb-3 p-1')
          resolution_container.appendChild(child)
      });

      const minutes_container = document.getElementById('minutes_container')
      document.getElementById('add_minutes_upload').addEventListener('click', function(){
        const minutes_child = document.createElement('input')
        minutes_child.setAttribute('type', 'file')
        minutes_child.setAttribute('name', 'minutes_file[]')
        minutes_child.setAttribute('class', 'form-control mb-3 p-1')
        minutes_container.appendChild(minutes_child)
      
      });

      const attendance_container = document.getElementById('attendance_container')
      document.getElementById('add_attendance_upload').addEventListener('click', function(){
        const attendance_child = document.createElement('input')
        attendance_child.setAttribute('type', 'file')
        attendance_child.setAttribute('name', 'attendance_file[]')
        attendance_child.setAttribute('class', 'form-control mb-3 p-1')
        attendance_container.appendChild(attendance_child)
      });

      const memo_container = document.getElementById('memo_container')
      document.getElementById('add_memo_upload').addEventListener('click', function(){
        const memo_child = document.createElement('input')
        memo_child.setAttribute('type', 'file')
        memo_child.setAttribute('name', 'memo_file[]')
        memo_child.setAttribute('class', 'form-control mb-3 p-1')
        memo_container.appendChild(memo_child)
      });

  </script>
@endsection

    


  

  


