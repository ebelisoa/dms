@extends('pages.admin.layout.layout')
@section('title', 'DMS-Dashboard')
@section('content')
<div class="container-fluid">
  <!-- Section: Main panel -->
 
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8 ">
      <!-- Card -->
        <div class="card">
          <div class="card-header" style="background-color: transparent; text-align:center;">
              <div class="row">
                <div class="col-md-2 d-flex align-items center justify-content-center">
                  <img src="{{ asset('img/Bagong_Pilipinas_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                </div>
                <div class="col-md-8 d-flex align-items center justify-content-center">
                  <h2><strong>ADMIN COUNCIL MEETING DETAILS</strong></h2>
                </div>
                <div class="col-md-2 d-flex align-items center justify-content-center">
                    <img src="{{ asset('img/dmmmsu_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                </div>
              </div>
          </div>
        </div>
      
      <div class="card card-cascade narrower d-flex content-justify-center">
        <div class="card card-header" style="background-color: transparent;">
            <div class="d-flex align-items-center justify-content-end">
                <a href="{{ route('admin.admin-council.index') }}">
                    <x-button-primary>Back</x-button-primary>
                </a>
            </div>
        </div>
        
        <div class="card-body">

          <div class="row">
            <div class="col-md-4">
              <h5>Document Type:</h5>
            </div>
            <div class="col-md-6">
              {{ strtoupper($meeting_details->document_type) }}
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <h5>
                Document Description
              </h5>
            </div>
            <div class="col-md-6">
                {{ $meeting_details->document_description }}
            </div>
          </div>

          @if(!empty($details))
            <div class="row">
                <div class="col-md-4">
                  <h5>No:</h5> 
                </div>
                <div class="col-md-6">
                  {{ $details->no }}
                </div>
            </div>
          @else

          @endif

          @if(!empty($details))
            <div class="row">
              <div class="col-md-4">
                <h5>Subject:</h5>
              </div>
              <div class="col-md-6">
                {{ $details->subject }}
              </div>
            </div>
          @else
          @endif


          <div class="row">
            <div class="col-md-4">
              <h5>Downloadable Files</h5>
            </div>
            <div class="col-md-6">
              @if(empty($files))
                No Uploaded Files
              @else

                @foreach ($files as $file )
                  @foreach ($file as $f )
                  
                    <div>
                      <div>
                        <a href="{{ route('admin.admin-council.download', ['id'=>$f->id]) }}">
                          {{ $f->name }}
                        </a>
                      </div>
                    </div>

                  @endforeach  
                @endforeach
                
              @endif
            </div>
           
          </div>

        </div>
      </div>
      <!-- Card -->
    </div>
    <div class="col-md-2"></div>
  </div>

</div>

@endsection
@section('script')
  <script>
    // SideNav Initialization
  $(".button-collapse").sideNav();

let container = document.querySelector('.custom-scrollbar');
var ps = new PerfectScrollbar(container, {
  wheelSpeed: 2,
  wheelPropagation: true,
  minScrollbarLength: 20
});

  </script>
@endsection

    


  

  


