@extends('pages.admin.layout.layout')
@section('title', 'DMS-Dashboard')
@section('content')
<div class="container-fluid">
 
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <!-- Card -->
        <div class="card mb-3">
          <div class="card-header" style="background-color: transparent; text-align:center;">
              <div class="row">
                <div class="col-md-2 text-center d-flex align-items-center justify-content-center">
                  <img src="{{ asset('img/Bagong_Pilipinas_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                  
                </div>
                <div class="col-md-8 text-center d-flex align-items-center justify-content-center">
                  <h2><strong>ADD FILES</strong></h2>
                </div>
                <div class="col-md-2 text-center d-flex align-items-center justify-content-center">
                  <img src="{{ asset('img/dmmmsu_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                </div>
              </div>
          </div>
        </div>


      <div class="card ">
        <div class="card card-header" style="background-color: transparent;">
          <div class="d-flex justify-content-end align-items-center">
            <a href="{{ route('admin.files.index', ['meeting_details' => $meeting_details_id ]) }}">
              <x-button-primary>Back</x-button-primary>
            </a>
          </div>
        </div>
        
        <div class="card-body">
          <div class="mt-2">
            @if (session()->has('success'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                  {{ session('success') }}
              </div>
            @elseif (session()->has('error'))  
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {{ session('error') }}
              </div>
            @else
              
            @endif
          </div>
          

          <form action="{{ route('admin.files.store') }}" method="post" enctype="multipart/form-data" >
            @csrf
            <input type="hidden" name="id" id="id" value="{{ $meeting_details_id }}">
            <div class="row mb-3">
              <label for="" class="col-md-3 col-form-label">File:
                <a id="add_file_upload" class="btn btn-success rounded-pill p-1">
                  <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width:25px; height:25px;">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                  </svg>
                </a>
              </label>
              <div class="col-md-9" id="file_container">
                  <input type="file" name="file[]" id="file[]" class="form-control mb-3">
              </div>
            </div>
            <div class="row mb-3 ">
              <div class="col-md-12 justify-content-center ">
                <div class="row justify-content-center">
                    <button type="submit" class="btn btn-success rounded-pill" style="width: 120px;" id="submit">Submit</button>
                    <button type="reset" class="btn btn-danger rounded-pill" style="width: 120px;">Reset</button>
                </div>
              </div>

            </div>

          </form>
        </div>
      </div>
      <!-- Card -->
    </div>
    <div class="col-md-2"></div>
  </div>

</div>

@endsection
@section('script')
  <script>
    // SideNav Initialization
    $(".button-collapse").sideNav();

    let container = document.querySelector('.custom-scrollbar');
    var ps = new PerfectScrollbar(container, {
        wheelSpeed: 2,
        wheelPropagation: true,
        minScrollbarLength: 20
    });
  

    const file_container = document.getElementById('file_container')
    document.getElementById('add_file_upload').addEventListener('click', function(){
        const child = document.createElement('input')
        child.setAttribute('type', 'file')
        child.setAttribute('name', 'file[]')
        child.setAttribute('class', 'form-control mb-3')
        file_container.appendChild(child)
    });

      /* const agenda_container = document.getElementById('agenda_container')
      document.getElementById('add_agenda_upload').addEventListener('click', function(){
          const child = document.createElement('input')
          child.setAttribute('type', 'file')
          child.setAttribute('name', 'agenda[]')
          child.setAttribute('class', 'form-control')
          agenda_container.appendChild(child)
      });

      const resolution_container = document.getElementById('resolution_container')
      document.getElementById('add_resolution_upload').addEventListener('click', function(){
          const child = document.createElement('input')
          child.setAttribute('type', 'file')
          child.setAttribute('name', 'resolution_file[]')
          child.setAttribute('class', 'form-control')
          resolution_container.appendChild(child)
      });

      const minutes_container = document.getElementById('minutes_container')
      document.getElementById('add_minutes_upload').addEventListener('click', function(){
        const minutes_child = document.createElement('input')
        minutes_child.setAttribute('type', 'file')
        minutes_child.setAttribute('name', 'minutes_file[]')
        minutes_child.setAttribute('class', 'form-control')
        minutes_container.appendChild(minutes_child)
      
      });

      const attendance_container = document.getElementById('attendance_container')
      document.getElementById('add_attendance_upload').addEventListener('click', function(){
        const attendance_child = document.createElement('input')
        attendance_child.setAttribute('type', 'file')
        attendance_child.setAttribute('name', 'attendance_file[]')
        attendance_child.setAttribute('class', 'form-control')
        attendance_container.appendChild(attendance_child)
      });

      const memo_container = document.getElementById('memo_container')
      document.getElementById('add_memo_upload').addEventListener('click', function(){
        const memo_child = document.createElement('input')
        memo_child.setAttribute('type', 'file')
        memo_child.setAttribute('name', 'memo_file[]')
        memo_child.setAttribute('class', 'form-control')
        memo_container.appendChild(memo_child)
      });  */

  </script>
@endsection

    


  

  


