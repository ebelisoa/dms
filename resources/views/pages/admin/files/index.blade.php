@extends('pages.admin.layout.layout')
@section('title', 'DMS-Dashboard')
@section('content')

    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2 text-center d-flex align-items-center justify-content-center">
                      <img src="{{ asset('img/Bagong_Pilipinas_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                      
                    </div>
                    <div class="col-lg-8 text-center d-flex align-items-center justify-content-center">
                        <div class="row">
                            <h2><strong>{{ $category }}</strong></h2>
                        </div>
                    </div>
                    <div class="col-md-2 text-center d-flex align-items-center justify-content-center">
                      <img src="{{ asset('img/dmmmsu_logo.png') }}" alt="" width="75px;" height="70px;" class="p-0">
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" style="background-color: transparent;">
                <div class="d-flex align-items-center justify-content-end">
                    @if ($category == 'Admin Council')
                        <a href="{{ route('admin.admin-council.index') }}">
                            <x-button-primary>Back</x-button-primary>
                        </a>
                    @elseif ($category == 'Academic Council')
                        <a href="{{ route('admin.academic-council.index') }}">
                            <x-button-primary>Back</x-button-primary>
                        </a>
                    @elseif ($category == 'Preboard Meeting')
                        <a href="{{ route('admin.preboard-meeting.index') }}">
                            <x-button-primary>Back</x-button-primary>
                        </a>
                    @elseif ($category == 'Regular Board Meeting')
                        <a href="{{ route('admin.regular-board-meeting.index') }}">
                            <x-button-primary>Back</x-button-primary>
                        </a>
                    @elseif ($category == 'Special Meeting')
                        <a href="{{ route('admin.special-meeting.index') }}">
                            <x-button-primary>Back</x-button-primary>
                        </a>
                    @endif

                    
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <strong>Document Type:</strong> {{ $meeting_details->document_type }}
                    </div>
                    <div class="col-md-12">
                        <strong>Document Description:</strong> {{ $meeting_details->document_type }}
                    </div>
                </div>
            </div>
           <div class="card-body">
                <div class="d-flex align-items-center justify-content-end mb-3 mr-3">
                    <a href="{{ route('admin.files.add-files', ['id' => $meeting_details->id]) }}">
                        <button class="btn btn-success rounded-pill p-2">
                            <strong>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width:20px; height: 20px;">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                                </svg>
                            </strong>
                        </button>
                    </a>
                </div>
                <table id="dtMaterialDesignExample" class="table table-striped" cellspacing="0" width="100%">
                    <tbody>
                        @if ($files == null)
                            <tr>
                                <td colspan="3">No Data Available</td>
                            </tr>
                        @else
                            
                        @endif
                        @foreach ($files as $item)
                            @foreach ($item as $items)
                                <tr>
                                    
                                    <td>{{ $items->name }}</td>
                                    <td>
                                        <a href="{{ route('admin.files.download', ['id' => $items->id]) }}">
                                            <button class="btn btn-success rounded-pill p-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width:20px; height: 20px;">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                                                  </svg>
                                                  
                                            </button>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal" data-target="#delete{{ $items->id }}">
                                            <button class="btn btn-danger rounded-pill p-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" style="width:20px; height: 20px;">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                                                </svg>
                                            </button>
                                        </a>
                                        <div class="modal fade" id="delete{{ $items->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-notify modal-info" role="document">
                                              <!-- Content -->
                                              <div class="modal-content">
                                                <!-- Header -->
                                                <div class="modal-header" style="background-color: #16a34a; ">
                                                  <p class="heading lead text-center">Delete File</p>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true" class="white-text">&times;</span>
                                                  </button>
                                                </div>
                      
                                                <!-- Body -->
                                                <div class="modal-body">
                                                               
                                                  Do you really want to delete these records? This process cannot be undone.
                                                  <form action="{{ route('admin.files.destroy', ['id' => $items->id ]) }}" method="post">
                                                    @method('delete')
                                                    @csrf
                                                </div>
                      
                                                <!-- Footer -->
                                                <div class="modal-footer justify-content-center">
                                                  <button type="submit" class="btn btn-outline-danger waves-effect" >Delete</button>
                                                  <button type="button" class="btn btn-outline-success waves-effect" data-dismiss="modal">Close</button>
                                                </div>
                                                  </form>
                                              </div>
                                              <!-- Content -->
                                            </div>
                                          </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
           </div>
        </div>
    </div>
@endsection
@section('script')
  
@endsection