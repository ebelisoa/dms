 <!-- Main Navigation -->
 <header>

    <!-- Sidebar navigation -->
    <div id="slide-out" class="side-nav sn-bg-4 fixed">
      <ul class="custom-scrollbar">

        <!-- Logo -->
        <li class="logo-sn waves-effect py-3">
          <div class="d-flex justify-content-center align-items-center">
            <a href="{{ route('admin.dashboard') }}" class="p-0 m-0 text-center">
              <img src="{{ asset('img/ub2.png') }}" alt="" width="75px" height="70px" >
            </a>
          </div>
        </li>
        <hr>

        <!-- Side navigation links -->
        <li>
          <ul class="collapsible collapsible-accordion">
            <!-- Simple link -->
            <li>
              <a href="{{ route('admin.dashboard') }}" class="collapsible-header waves-effect {{ request()->is('admin.dashboard' ? 'active' : '') }}">Dashboard</a>
            </li>
            <li>
              <a href="{{ route('admin.admin-council.index') }}" class="collapsible-header waves-effect{{ request()->is('/admin/admincouncil/*' ? 'active' : '') }}">Admin Council</a>
            </li>
            <li>
              <a href="{{ route('admin.academic-council.index') }}" class="collapsible-header waves-effect">Academic Council</a>
            </li>
            <li>
              <a href="{{ route('admin.preboard-meeting.index') }}" class="collapsible-header waves-effect">Preboard Meeting</a>
            </li>
            <li>
              <a href="{{ route('admin.regular-board-meeting.index') }}" class="collapsible-header waves-effect">Regular Board Meeting</a>
            </li>
            <li>
              <a href="{{ route('admin.special-meeting.index') }}" class="collapsible-header waves-effect">Special Meeting</a>
            </li>
            <li>
              <a href="{{ route('admin.reports.index') }}" class="collapsible-header waves-effect">Reports</a>
            </li>
          </ul>
        </li>
        <!-- Side navigation links -->

      </ul>
      <div class="sidenav-bg mask-strong"></div>
    </div>
    <!-- Sidebar navigation -->

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg scrolling-navbar double-nav">

      <!-- SideNav slide-out button -->
      <div class="float-left">
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i></a>
      </div>

      <!-- Breadcrumb -->
      <div class="breadcrumb-dn mr-auto">
        <p><b>UniBoss - Document Management System</b></p>
      </div>

      <div class="d-flex change-mode">
        <!-- Navbar links -->
        <ul class="nav navbar-nav nav-flex-icons ml-auto">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-user"></i> <span class="clearfix d-none d-sm-inline-block">Profile</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="{{ route('admin.user-account.index') }}">My account</a>
              <a class="dropdown-item" href="{{ route('logout') }}">Log Out</a>
              
            </div>
          </li>

        </ul>
        <!-- Navbar links -->

      </div>

    </nav>
    <!-- Navbar -->


  </header>
  <!-- Main Navigation -->