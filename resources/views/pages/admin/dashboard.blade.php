@extends('pages.admin.layout.layout')
@section('title', 'DMS-Dashboard')
@section('content')
<div class="container-fluid ">
  <div class="row justify-content-center">
    <a href="{{ route('admin.admin-council.index') }}" style="text-decoration: none; color:black;">
      <div class="card m-1" style="width: 17rem;">
        <div class="card-body">
          <h5 class="card-title">Admin Council</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary">Total Meeting</h6>
          <p class="card-text">{{ $adminTotal }}</p>
        </div>
      </div>
    </a>


      <a href="{{ route('admin.academic-council.index') }}" style="text-decoration: none; color:black;">
        <div class="card m-1" style="width: 17rem;">
          <div class="card-body">
            <h5 class="card-title">Academic Council</h5>
            <h6 class="card-subtitle mb-2 text-body-secondary">Total Meeting</h6>
            <p class="card-text">{{ $academicTotal }}</p>
          </div>
        </div>
      </a>

    <a href="{{ route('admin.preboard-meeting.index') }}" style="text-decoration: none; color:black;">
      <div class="card m-1" style="width: 17rem;">
        <div class="card-body">
          <h5 class="card-title">Pre Board Meeting</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary">Total Meeting</h6>
          <p class="card-text">{{ $preboardTotal }}</p>     
        </div>
      </div>
    </a>
      
    <a href="{{ route('admin.regular-board-meeting.index') }}" style="text-decoration: none; color:black;">
      <div class="card m-1" style="width: 17rem;">
        <div class="card-body">
          <h5 class="card-title">Regular Board Meeting</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary">Total Meeting</h6>
          <p class="card-text">{{ $regularBoardTotal }}</p>
        </div>
      </div>
    </a>

    <a href="{{ route('admin.special-meeting.index') }}" style="text-decoration: none; color:black;">
      <div class="card m-1" style="width: 17rem;">
        <div class="card-body">
          <h5 class="card-title">Special Meeting</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary">Total Meeting</h6>
          <p class="card-text">{{ $specialTotal }}</p>
        </div>
      </div>
    </a>

  </div>

  <div class="container-fluid">
    <div class="card">
      <div class="card-body">
          <canvas id="myChart"></canvas>
      </div>
    </div>
  </div>
  
</div>

@endsection

@section('script')
  <script>
    


    // SideNav Initialization
    $(".button-collapse").sideNav();
    let container = document.querySelector('.custom-scrollbar');
    var ps = new PerfectScrollbar(container, {
      wheelSpeed: 2,
      wheelPropagation: true,
      minScrollbarLength: 20
    });

    const ctx = document.getElementById('myChart');
    $.ajax({
      type: 'get',
      url: '{{route("admin.get.meetingTotals")}}',
      dataType: 'json',
      success: function(data){
      
        var admin = data.admin;
        var academic  = data.academic;
        var preboard = data.preboard;
        var regularboard = data.regularboard;
        var special = data.special;

        new Chart(ctx,{
          type: 'bar',
          data: {
            labels: ['Academic Council', 'Admin Council', 'Preboard Meeting', 'Regular Board Meeting', 'Special Meeting'],
            datasets: [{
              label: 'Total Meetings',
              data: [admin, academic, preboard, regularboard, special],
              borderWidth: 1,
              backgroundColor:['#9BD0F5','#ec2151','#0003d7','#2da5ad','#ad2d89'],
            }]
          },
        
          options: {
            scales: {
              y: {
                beginAtZero: true
              }
            }
          },



        });
        
      },
    });
   
    
     

  </script>
@endsection

    


  

  


