<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>@yield('title', 'DMS')</title>
  <link rel="icon" type="image/x-icon" href="{{ asset('img/dmmmsu_logo.png') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="{{ asset('css/own-style.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="{{ asset('css/mdb.min.css')}}">
  <!-- Your custom styles (optional) -->
  
  <link rel="stylesheet" href="{{ asset('css/addons/datatables.css') }}">
  <link rel="stylesheet" href="{{ asset('css/addons/datatables.min.css') }}">
  
  <style type="text/javascript" src="{{ asset('js/swal2.js') }}"></style>

  <style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Firefox */
    input[type=number] {
      -moz-appearance: textfield;
    }

  </style>
</head>

<body class="fixed-sn white-skin">

  @include('pages.admin.inc.header')
  <main>
      @yield('content')
  </main>
  {{--    @include('pages.admin.inc.footer') --}}
   

      <!-- JQuery  -->
      <script src="{{ asset('js/jquery-3.7.1.min.js') }}"></script>
      <!-- Bootstrap tooltips  -->
      <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
      <!-- Bootstrap core JavaScript  -->
      <script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
      <!-- MDB core JavaScript  -->
      <script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>
      <!-- DataTables  -->
      <script type="text/javascript" src="{{ asset('js/addons/datatables.min.js') }}"></script>
      <!-- DataTables Select  -->
      <script type="text/javascript" src="{{ asset('js/addons/datatables-select.min.js') }}"></script>
  <!-- Custom scripts -->

    @yield('script')
    <!-- Initializations -->
    <script>
       

    </script>
</body>
</html>