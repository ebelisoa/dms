<?php

use App\Http\Controllers\AcademicCouncilController;
use App\Http\Controllers\AdminCouncilController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FilesController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\PreboardMeetingController;
use App\Http\Controllers\RegularBoardMeetingController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\SpecialMeetingController;
use App\Http\Controllers\Superadmin\CategoryController;
use App\Http\Controllers\Superadmin\DashboardController as SuperadminDashboardController;
use App\Http\Controllers\Superadmin\DocumentTypeController;
use App\Http\Controllers\Superadmin\UsersController as SuperadminUsersController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/* Route::get('/', [IndexController::class, 'index'])->name('home'); */
Route::get('/index', [IndexController::class, 'index'])->name('index');

Route::post('/index', [AuthController::class, 'login'])->name('login');


/* Admin */
Route::prefix('admin')->name('admin.')->group(function(){

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/d',[DashboardController::class, 'getData'])->name('get.meetingTotals');
    Route::get('/getCategory/{id}', [CategoryController::class,'fetch']);
    Route::post('/dashboard', [DashboardController::class, 'store'])->name('store.meetingDetails');
    

    Route::controller(AdminCouncilController::class)->group(function(){
        Route::get('/admincouncil', 'index')
            ->name('admin-council.index');
        Route::get('/adminCouncilCreate/{id}', 'create')
            ->name('admin-council.create');
        Route::post('/adminCouncilCreate', 'store')
            ->name('admin-council.store');
        Route::get('/adminCouncilEdit/{id}','edit')
            ->name('admin-council.edit');
        Route::post('/adminCouncilUpdate', 'update')
            ->name('admin-council.update');
        Route::delete('/adminCouncilDelete/{id}','destroy')
            ->name('admin-council.destroy');
        Route::get('/adminCouncilDownload/{id}', 'download')
            ->name('admin-council.download');
        Route::get('/adminCouncilMeetingDetails/{id}', 'meetingDetails')
            ->name('admin-council.meeting-details');
    });

    Route::controller(AcademicCouncilController::class)->group(function(){
        Route::get('/AcademicCouncil', 'index')
            ->name('academic-council.index');
        Route::get('/AcademicCouncilCreate/{id}', 'create')
            ->name('academic-council.create');
        Route::post('/AcademicCouncilCreate', 'store')
            ->name('academic-council.store');
        Route::get('/AcademicCouncilEdit/{id}', 'edit')
            ->name('academic-council.edit');
        Route::post('/AcademicCOuncilUpdate', 'update')
            ->name('academic-council.update');
        Route::delete('/AcademicCouncilDelete/{id}', 'destroy')
            ->name('academic-council.destroy');
        Route::get('/AcademicCouncilMeetingDetails/{id}', 'meeting')
            ->name('academic-council.meeting-details');
        Route::get('/AcademicCouncilDownload/{id}', 'download')
            ->name('academic-council.download');
    });
    
    /* Academic Council -> Remove File Uploaded */
    Route::post('/updateAcademic', [AcademicCouncilController::class,'remove'])->name('remove.academiccouncil');

    Route::controller(PreboardMeetingController::class)->group(function(){
        Route::get('/PreboardMeeting', 'index')
            ->name('preboard-meeting.index');
        Route::get('/PreboardMeetingCreate/{id}', 'create')
            ->name('preboard-meeting.create');
        Route::post('/PreboardMeetingStore', 'store')
            ->name('preboard-meeting.store');
        Route::get('/PreboardMeetingEdit/{id}', 'edit')
            ->name('preboard-meeting.edit');
        Route::post('/PreboardMeetingUpdate', 'update')
            ->name('preboard-meeting.update');
        Route::delete('/PreboardMeetingDestroy/{id}', 'destroy')
            ->name('preboard-meeting.destroy');
        Route::get('/PreboardMeetingMeetingDetails/{meeting_details}', 'meeting')
            ->name('preboard-meeting.meeting-details');
        Route::get('/PreboardMeetingDownload/{id}', 'download')
            ->name('preboard-meeting.download');
    });

    Route::controller(RegularBoardMeetingController::class)->group(function () {
        Route::get('/RegularBoardMeeting', 'index')
            ->name('regular-board-meeting.index');
        Route::get('/RegularBoardMeetingCreate/{id}', 'create')
            ->name('regular-board-meeting.create');
        Route::post('/RegularBoardMeetingStore', 'store')
            ->name('regular-board-meeting.store');
        Route::get('/RegularBoardMeetingEdit/{meeting_details}', 'edit')
            ->name('regular-board-meeting.edit');
        Route::post('/RegularBoardMeetingUpdate', 'update')
            ->name('regular-board-meeting.update');
        Route::delete('/RegularBoardMeetingDestroy/{id}', 'destroy')
            ->name('regular-board-meeting.destroy');
        Route::get('/RegularBoardMeetingMeetingDetails/{id}', 'meeting')
            ->name('regular-board-meeting.meeting-details');
        Route::get('/RegularBoardMeetingDownload/{id}', 'download')
            ->name('regular-board-meeting.download');
            
    });

    Route::controller(SpecialMeetingController::class)->group(function () {
        Route::get('/SpecialMeeting', 'index')
            ->name('special-meeting.index');
        Route::get('/SpecialMeetingCreate/{id}', 'create')
            ->name('special-meeting.create');
        Route::post('/SpecialMeetingStore', 'store')
            ->name('special-meeting.store');
        Route::get('/SpecialMeetingEdit/{meeting_details}', 'edit')
            ->name('special-meeting.edit');
        Route::post('/SpecialMeetingUpdate', 'update')
            ->name('special-meeting.update');
        Route::delete('/SpecialMeetingDestroy/{id}', 'destroy')
            ->name('special-meeting.destroy');

        Route::get('/SpecialMeetingMeetingDetails/{meeting_details}', 'meeting')
            ->name('special-meeting.meeting-details');
        Route::get('/SpecialMeetingMeetingDownload/{id}', 'download')
            ->name('special-meeting.download');
    });

    Route::controller(UsersController::class)->group(function () {
        Route::get('/UserAccount', 'index')
            ->name('user-account.index');
        Route::put('/UserAccount/{user_account}', 'update')
            ->name('user-account.update');
    });

    Route::controller(ReportsController::class)->group(function () {
        Route::get('/reports', 'index')
            ->name('reports.index');
        Route::get('/ViewReports/{id}', 'viewReports')
            ->name('view-reports.view-reports');
        Route::post('/ViewReports', 'fetchData')
            ->name('view-reports.reports');
        Route::get('/filter', 'filter_year')
            ->name('filter_year.reports');
        Route::post('/printPDF', 'print_pdf')
            ->name('print_pdf.reports');
    });

    Route::controller(FilesController::class)->group(function () {
        Route::get('/Files/{meeting_details}', 'index')
            ->name('files.index');
        Route::get('/Files/{id}/Add_File', 'add_files')
            ->name('files.add-files');
        Route::post('/Files/store', 'store')
            ->name('files.store');
        Route::get('/FilesDownload/{id}', 'download')
            ->name('files.download');
        Route::delete('/DeleteFiles/{id}', 'destroy')
            ->name('files.destroy');
    });

  
});



/* Superadmin */

Route::prefix('superadmin')->name('superadmin.')->group(function(){
    
    Route::get('/dashboard', [SuperadminDashboardController::class, 'index'])->name('index.dashboard');

    /* Category */

    Route::get('/manageCategory', [CategoryController::class, 'index'])->name('index.category');
    Route::get('/createCategory', [CategoryController::class, 'create'])->name('create.category');
    Route::post('/createCategory', [CategoryController::class, 'store'])->name('store.category');

    /* Document Type */

    Route::get('/manageDocumentType', [DocumentTypeController::class, 'index'])->name('index.documenttype');
    Route::get('/createDocumentType', [DocumentTypeController::class, 'create'])->name('create.documenttype');
    Route::post('/createDocumentType', [DocumentTypeController::class, 'store'])->name('store.documenttype');
    Route::get('/{id}/updateDocumentType', [DocumentTypeController::class,'edit'])->name('edit.documenttype');
    Route::put('/{id}/updateDocumentType', [DocumentTypeController::class,'update'])->name('update.documenttype');

    /* Users Management */
    Route::get('/users', [SuperadminUsersController::class, 'index'])->name('index.users');
    Route::get('/createUsers', [SuperadminUsersController::class,'create'])->name('create.users');
    Route::post('/createUsers', [SuperadminUsersController::class,'store'])->name('store.users');
    Route::get('/{id}/updateUsers', [SuperadminUsersController::class, 'edit'])->name('edit.users');

});

/* Logout */

Route::get('/', [AuthController::class, 'logout'])->name('logout');