<?php

namespace App\Http\Controllers;

use App\Http\Requests\SpecialMeetingRequest;
use App\Http\Requests\SpecialMeetingRequestUpdate;
use App\Http\Requests\StoreSpecialMeetingRequest;
use App\Http\Requests\UpdateSpecialMeetingRequest;
use App\Models\Details;
use App\Models\DocumentFiles;
use App\Models\File;
use App\Models\MeetingDetails;
use App\Models\Superadmin\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class SpecialMeetingController extends Controller
{
    public function index(){

        $data = MeetingDetails::join('category', 'meeting_details.category_id', '=', 'category.id')
        ->where('category.categoryName', 'Special Meeting')
        ->get('meeting_details.*');

        $category = Category::where('categoryName', 'Special Meeting')->first('id');     
        return view('pages.admin.special-meeting.index',[
            'meeting_details' => $data,
            'category' => $category, 
        ]);

    }

    public function create(string $id){
        return view('pages.admin.special-meeting.create', [
            'category' => $id,
        ]);
    }

    public function store(StoreSpecialMeetingRequest $request){

        $context = [
            'meeting_date' => $request->validated('meeting_date'),
            'meeting_time' => $request->validated('meeting_time'),
            'category_id' => $request->validated('category_id'),
            'document_type' => $request->validated('document_type'),
            'document_description' => $request->validated('document_description'),
            'quarter' => $request->validated('quarter'),
            'remarks' => $request->validated('remarks'),
        ];

        $stmt = MeetingDetails::create($context);
        $lid = $stmt->id;
      
        if($request->validated('document_type') == 'agenda' && $request->agenda != null){
            $agenda_file_id = array();
            
            foreach($request->agenda AS $f){
                $agenda_file_name = 'specialmeeting_agenda'.'_'.$f->getClientOriginalName();
                $path_file = $f->storeAs('public/uploads', $agenda_file_name);
                $files = File::create([
                    'name' => $agenda_file_name,
                    'file_path' => $path_file,
                ]);
                
                $agenda_file_id[] = $files->id;
             
            }

            foreach($agenda_file_id as $id){
               DocumentFiles::create([
                        'meeting_details_id' => $lid,
                        'file_id' => $id,
                ]);
            }
        }elseif($request->document_type == 'resolution' && $request->resolution_file != null){

            $resolution_file_id = array();

            if(empty($request->resolution_number) AND empty($request->resolution_subject)){
    
            }else{

                foreach($request->resolution_file AS $f){
                    $resolution_file_name = 'specialmeeting_resolution'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $resolution_file_name);
                    $files = File::create([
                        'name' => $resolution_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $resolution_file_id[] = $files->id;
                 
                }
    
                foreach($resolution_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }

                Details::create([
                    'meeting_details_id' => $lid,
                    'no' => $request->resolution_number,
                    'subject' => $request->resolution_subject,
                ]);
            }
        }elseif($request->document_type == 'minutes' && $request->minutes_file != null){
            $minutes_file_id = array();
            
            foreach($request->minutes_file AS $f){
                $minutes_file_name = 'specialmeeting_minutes'.'_'.$f->getClientOriginalName();
                $path_file = $f->storeAs('public/uploads', $minutes_file_name);
                $files = File::create([
                    'name' => $minutes_file_name,
                    'file_path' => $path_file,
                ]);
                
                $minutes_file_id[] = $files->id;
             
            }

            foreach($minutes_file_id as $id){
               DocumentFiles::create([
                        'meeting_details_id' => $lid,
                        'file_id' => $id,
                ]);
            }
        }elseif($request->validated('document_type') == 'attendance' && $request->attendance_file != null){
            $attendance_file_id = array();
            
            foreach($request->attendance_file AS $f){
                $attendance_file_name = 'specialmeeting_attendance'.'_'.$f->getClientOriginalName();
                $path_file = $f->storeAs('public/uploads', $attendance_file_name);
                $files = File::create([
                    'name' => $attendance_file_name,
                    'file_path' => $path_file,
                ]);
                
                $attendance_file_id[] = $files->id;
             
            }

            foreach($attendance_file_id as $id){
               DocumentFiles::create([
                        'meeting_details_id' => $lid,
                        'file_id' => $id,
                ]);
            }
        }elseif($request->validated('document_type') == 'memo' && $request->memo_file != null){
            $memo_file_id = array();

            if(empty($request->memo_number) AND empty($request->memo_subject)){
    
            }else{

                foreach($request->memo_file AS $f){
                    $memo_file_name = 'specialmeeting_memo'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $memo_file_name);
                    $files = File::create([
                        'name' => $memo_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $memo_file_id[] = $files->id;
                 
                }
    
                foreach($memo_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }

                Details::create([
                    'meeting_details_id' => $lid,
                    'no' => $request->memo_number,
                    'subject' => $request->memo_subject,
                ]);
            }
        }elseif($request->document_type == 'documentation' && $request->image_file != null ){

            if($request->image_file != null && $request->validated('link') != null){
                foreach($request->image_file AS $f){
                    $image_file_name = 'specialmeeting_documentation'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $image_file_name);
                    $files = File::create([
                        'name' => $image_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $image_file_id[] = $files->id;
                 
                }
    
                foreach($image_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }

                Details::create([
                    'meeting_details_id' => $lid,
                    'subject' => $request->link,
                ]);
            }elseif($request->image_file != null && $request->validated('link') == null ){
                foreach($request->image_file AS $f){
                    $image_file_name = 'specialmeeting_documentation'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $image_file_name);
                    $files = File::create([
                        'name' => $image_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $image_file_id[] = $files->id;
                 
                }
    
                foreach($image_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }
            }else{
                Details::create([
                    'meeting_details_id' => $lid,
                    'subject' => $request->link,
                ]);
            }
        }else{

        }

        if(!$stmt){
            return redirect()->intended(route('admin.special-meeting.index'))->with('error', 'Saving Failed');
        }
        return redirect()->intended(route('admin.special-meeting.index'))->with('success', 'Saving Successful');
    }

    public function edit(MeetingDetails $meeting_details){

        $additional_details = Details::where('meeting_details_id', $meeting_details->id)->first();
        $file_details = DocumentFiles::join('files', 'files.id', 'document_files.file_id')->where('document_files.id', $meeting_details->id)->get();

        if(empty($additional_details)){
            $no = '';
            $subject = '';
        }else{
            $no = $additional_details->no;
            $subject = $additional_details->subject;
        }   


        $category = Category::where('categoryName', 'Academic Council')->first('id');
        return view('pages.admin.special-meeting.edit',[
            'meeting_details' => $meeting_details,
            'no' => $no,
            'subject' =>$subject,
            'category' => $category,
            'file_details' => $file_details,
        ]);

    }

    public function update(UpdateSpecialMeetingRequest $request){

        $context = [
            'meeting_date' => $request->validated('meeting_date'),
            'meeting_time' => $request->validated('meeting_time'),
            'document_type' => $request->validated('document_type'),
            'document_description' => $request->validated('document_description'),
            'quarter' => $request->validated('quarter'),
            'remarks' => $request->validated('remarks'),
        ];

        $update_meeting_details = MeetingDetails::where('id', $request->validated('meeting_details_id'))->update($context);

        if($request->validated('document_type') == 'resolution'){
            Details::where('meeting_details_id', $request->validated('meeting_details_id'))->update([
                'no' => $request->resolution_number,
                'subject' => $request->resolution_subject,
            ]); 
        }elseif($request->validated('document_type') == 'memo'){
           Details::where('meeting_details_id', $request->validated('meeting_details_id'))->update([
                'no' => $request->memo_number,
                'subject' => $request->memo_subject,
            ]); 
        }elseif($request->validated('document_type') == 'documentation'){
            Details::where('meeting_details_id',$request->validated('meeting_details_id'))->update([
                'subject' => $request->validated('link'),
            ]); 
        }else{

        }

        if(!$update_meeting_details){
            return redirect()->intended(route('admin.special-meeting.index'))->with('failed', 'Update Failed!, something wrong happened!');
        }else{
            return redirect()->intended(route('admin.special-meeting.index'))->with('success', 'Update Successful');
        }
    }


    public function destroy(string $id){

        $data = DocumentFiles::where('meeting_details_id', $id)->get();

        foreach($data as $d){
            $file_id = $d->file_id;
            $file = File::where('id', $file_id)->first();
            Storage::delete($file->file_path);
            File::where('id', $file_id)->delete();
            DocumentFiles::where('meeting_details_id',$id)->where('file_id',$file_id)->delete();
        }

        $stmt = MeetingDetails::where('id', $id)->delete();
        if(!$stmt){
            return redirect()->intended(route('admin.special-meeting.index'))->with('error', 'Delete Failed');
        }else{
            return redirect()->intended(route('admin.special-meeting.index'))->with('success', 'Delete Successful!');
        }
        

    }

    public function download(string $id){

        $stmt = File::where('id', $id)->first();
        $data = Storage::get($stmt->file_path);
        
        $response = Response::make($data, 200);
        $response->header('Content-Type', 'application/pdf');
        return $response;

    }


    public function meeting(MeetingDetails $meeting_details){

        /* $meeting_details =  MeetingDetails::where('id', $id->id)->first(); */
        $category = Category::where('id', $meeting_details->category_id)->first();
        $document_files = DocumentFiles::where('meeting_details_id', $meeting_details->id)->get();
        $details = Details::where('meeting_details_id', $meeting_details->id)->first();
        
        if($document_files->count()){
            foreach($document_files AS $df){
                $files[] = File::WHERE('id', $df->file_id)->get();
            }
        }else{
            $files = '';
        }

        return view('pages.admin.special-meeting.meeting-details',[
            'meeting_details' => $meeting_details,
            'category'=> $category,
            'files' => $files,
            'details' => $details,
        ]);
      
    }
}
