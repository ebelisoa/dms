<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminCouncilRequest;
use App\Http\Requests\AdminCouncilRequestStore;
use App\Models\Details;
use App\Models\DocumentFiles;
use App\Models\File;
use App\Models\MeetingDetails;
use App\Models\Superadmin\Category;
use App\Models\Superadmin\DocumentTypes;
use Faker\Guesser\Name;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class AdminCouncilController extends Controller
{
    public function index(){
        
        $data = MeetingDetails::join('category', 'meeting_details.category_id', '=', 'category.id')
                ->where('category.categoryName', 'Admin Council')
                ->orderBy('created_at', 'desc')
                ->get('meeting_details.*');

        $admin_council = Category::where('categoryName', 'Admin Council')->first('id');
        
        return view('pages.admin.admin-council.index',[
            'meeting_details' => $data,
            'admin_council_id' => $admin_council,
        ]);
    }

    public function create(string $id){
        $document_type = DocumentTypes::all();

        return view('pages.admin.admin-council.create', [
            'category' => $id,
            'documenttype'=>$document_type, 
        ]);
    }

    public function store(AdminCouncilRequestStore $request){
        $context = [
            'meeting_date' => $request->validated('meeting_date'),
            'meeting_time' => $request->validated('meeting_time'),
            'category_id' => $request->validated('category_id'),
            'document_type' => $request->validated('document_type'),
            'document_description' => $request->validated('document_description'),
            'quarter' => $request->validated('quarter'),
            'remarks' => $request->validated('remarks'),
        ];

      
        $stmt = MeetingDetails::create($context);
        $lid = $stmt->id;
   
        if($request->document_type == 'agenda' && $request->agenda != null){
            if(empty($request->agenda)){

            }else{
                $agenda_file_id = array();
            
                foreach($request->agenda AS $f){
                    $agenda_file_name = 'admincouncil_agenda'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $agenda_file_name);
                    $files = File::create([
                        'name' => $agenda_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $agenda_file_id[] = $files->id;
                
                }

                foreach($agenda_file_id as $id){
                DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }
            }
            
        }elseif($request->document_type == 'resolution' && $request->resolution_file != null ){

            $resolution_file_id = array();

            if(empty($request->resolution_number) AND empty($request->resolution_subject)){
    
            }else{

                foreach($request->resolution_file AS $f){
                    $resolution_file_name = 'admincouncil_resolution'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $resolution_file_name);
                    $files = File::create([
                        'name' => $resolution_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $resolution_file_id[] = $files->id;
                 
                }
    
                foreach($resolution_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }

                Details::create([
                    'meeting_details_id' => $lid,
                    'no' => $request->resolution_number,
                    'subject' => $request->resolution_subject,
                ]);
            }
        }elseif($request->document_type == 'minutes' && $request->minutes_file != null){
            $minutes_file_id = array();
            
            foreach($request->minutes_file AS $f){
                $minutes_file_name = 'admincouncil_minutes'.'_'.$f->getClientOriginalName();
                $path_file = $f->storeAs('public/uploads', $minutes_file_name);
                $files = File::create([
                    'name' => $minutes_file_name,
                    'file_path' => $path_file,
                ]);
                
                $minutes_file_id[] = $files->id;
             
            }

            foreach($minutes_file_id as $id){
               DocumentFiles::create([
                        'meeting_details_id' => $lid,
                        'file_id' => $id,
                ]);
            }
        }elseif($request->document_type == 'attendance' && $request->attendance_file != null){
            $attendance_file_id = array();
            
            foreach($request->attendance_file AS $f){
                $attendance_file_name = 'admincouncil_attendance'.'_'.$f->getClientOriginalName();
                $path_file = $f->storeAs('public/uploads', $attendance_file_name);
                $files = File::create([
                    'name' => $attendance_file_name,
                    'file_path' => $path_file,
                ]);
                
                $attendance_file_id[] = $files->id;
             
            }

            foreach($attendance_file_id as $id){
               DocumentFiles::create([
                        'meeting_details_id' => $lid,
                        'file_id' => $id,
                ]);
            }
        }elseif($request->document_type == 'memo' && $request->memo_file != null){
            $memo_file_id = array();

            if(empty($request->memo_number) AND empty($request->memo_subject)){
    
            }else{

                foreach($request->memo_file AS $f){
                    $memo_file_name = 'admincouncil_memo'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $memo_file_name);
                    $files = File::create([
                        'name' => $memo_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $memo_file_id[] = $files->id;
                 
                }
    
                foreach($memo_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }

                Details::create([
                    'meeting_details_id' => $lid,
                    'no' => $request->memo_number,
                    'subject' => $request->memo_subject,
                ]);
            }
        }elseif($request->document_type == 'documentation' && $request->image_file != null){

            if(empty($request->link)){
                foreach($request->image_file AS $f){
                    $image_file_name = 'admincouncil_documentation'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $image_file_name);
                    $files = File::create([
                        'name' => $image_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $image_file_id[] = $files->id;
                 
                }
    
                foreach($image_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }
            }else{
                Details::create([
                    'meeting_details_id' => $lid,
                    'subject' => $request->link,
                ]);
            }

        }else{
            
        }

        if(!$stmt){
            return redirect()->intended(route('admin.admin-council.index'))->with('error', 'Saving Failed');
        }
        return redirect()->intended(route('admin.admin-council.index'))->with('success', 'Saving Successful');

}

    public function edit(string $id){

        $meeting_details = MeetingDetails::where('id', $id)->first();
        $additional_details = Details::where('meeting_details_id', $id)->first();
        $file_details = DocumentFiles::join('files', 'files.id', 'document_files.file_id')->where('document_files.id', $id)->get();

    
        if(empty($additional_details)){
            $no = '';
            $subject = '';
        }else{
            $no = $additional_details->no;
            $subject = $additional_details->subject;
        }   


       $category = Category::where('categoryName', 'Admin Council')->first();

        return view('pages.admin.admin-council.edit',[
            'meeting_details' => $meeting_details,
            'no' => $no,
            'subject' =>$subject,
            'file_details' => $file_details,
            'category' => $category,
        ]);

    }

    public function update(AdminCouncilRequest $request){
        $context = [
            'meeting_date' => $request->validated('meeting_date'),
            'meeting_time' => $request->validated('meeting_time'),
            'category_id' => $request->validated('category_id'),
            'document_type' => $request->validated('document_type'),
            'document_description' => $request->validated('document_description'),
            'quarter' => $request->validated('quarter'),
            'remarks' => $request->validated('remarks'),
        ];

        $stmt = MeetingDetails::where('id', $request->meeting_details_id)->update($context);
        $lid = $request->validated('meeting_details_id');
        if($request->document_type == 'agenda' && $request->agenda != null){
            if(empty($request->agenda)){

            }else{
                $agenda_file_id = array();
            
                foreach($request->agenda AS $f){
                    $agenda_file_name = 'admincouncil_agenda'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $agenda_file_name);
                    $files = File::create([
                        'name' => $agenda_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $agenda_file_id[] = $files->id;
                
                }

                foreach($agenda_file_id as $id){
                DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }
            }
            
        }elseif($request->document_type == 'resolution' && $request->resolution_file != null){

            $resolution_file_id = array();

            if(empty($request->resolution_number) AND empty($request->resolution_subject)){
    
            }else{

                foreach($request->resolution_file AS $f){
                    $resolution_file_name = 'admincouncil_resolution'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $resolution_file_name);
                    $files = File::create([
                        'name' => $resolution_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $resolution_file_id[] = $files->id;
                 
                }
    
                foreach($resolution_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }

                Details::create([
                    'meeting_details_id' => $lid,
                    'no' => $request->resolution_number,
                    'subject' => $request->resolution_subject,
                ]);
            }
        }elseif($request->document_type == 'minutes' && $request->minutes_file != null){
            $minutes_file_id = array();
            
            foreach($request->minutes_file AS $f){
                $minutes_file_name = 'admincouncil_minutes'.'_'.$f->getClientOriginalName();
                $path_file = $f->storeAs('public/uploads', $minutes_file_name);
                $files = File::create([
                    'name' => $minutes_file_name,
                    'file_path' => $path_file,
                ]);
                
                $minutes_file_id[] = $files->id;
             
            }

            foreach($minutes_file_id as $id){
               DocumentFiles::create([
                        'meeting_details_id' => $lid,
                        'file_id' => $id,
                ]);
            }
        }elseif($request->document_type == 'attendance' && $request->attendance_file != null){
            $attendance_file_id = array();
            
            foreach($request->attendance_file AS $f){
                $attendance_file_name = 'admincouncil_attendance'.'_'.$f->getClientOriginalName();
                $path_file = $f->storeAs('public/uploads', $attendance_file_name);
                $files = File::create([
                    'name' => $attendance_file_name,
                    'file_path' => $path_file,
                ]);
                
                $attendance_file_id[] = $files->id;
             
            }

            foreach($attendance_file_id as $id){
               DocumentFiles::create([
                        'meeting_details_id' => $lid,
                        'file_id' => $id,
                ]);
            }
        }elseif($request->document_type == 'memo' && $request->memo_file != null){
            $memo_file_id = array();

            if(empty($request->memo_number) AND empty($request->memo_subject)){
    
            }else{

                foreach($request->memo_file AS $f){
                    $memo_file_name = 'admincouncil_memo'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $memo_file_name);
                    $files = File::create([
                        'name' => $memo_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $memo_file_id[] = $files->id;
                 
                }
    
                foreach($memo_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }

                Details::create([
                    'meeting_details_id' => $lid,
                    'no' => $request->memo_number,
                    'subject' => $request->memo_subject,
                ]);
            }
        }elseif($request->document_type == 'documentation' && $request->image_file != null){

            if(empty($request->link)){
                foreach($request->image_file AS $f){
                    $image_file_name = 'admincouncil_documentation'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $image_file_name);
                    $files = File::create([
                        'name' => $image_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $image_file_id[] = $files->id;
                 
                }
    
                foreach($image_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }
            }else{
                Details::create([
                    'meeting_details_id' => $lid,
                    'subject' => $request->link,
                ]);
            }

        }else{
            
        }


        if(!$stmt){
            return redirect()->intended(route('admin.admin-council.edit', ['id' => $request->validated('meeting_details_id')]))->with('error', 'Update Failed');
        }else{
            return redirect()->intended(route('admin.admin-council.index'))->with('success', 'Update Successful'); 
        }




        
       /*  $data = $request->validated();

        $m = '';
        $a = '';
        $m2 = '';
        $d = '';

        $minute_file_id = $id->minute_file_id;
        $attendance_file_id = $id->attendance_file_id;
        $memo_file_id = $id->memo_file_id;
        $document_file_id = $id->document_file_id; 

        if(!empty($request->minute_file)){
            $minuteName = 'admincouncil_meeting'.'_'.$request->minute_file->getClientOriginalName();
            $chk_exist = File::where('name' , $minuteName)->first();
            if(!empty($chk_exist)){
                $m = 1;
            }
        }else{
            $m = 0;
        }


        if(!empty($request->attendance_file)){
            $attendanceName = 'admincouncil_meeting'.'_'.$request->attendance_file->getClientOriginalName();
            $chk_exist = File::where('name' , $attendanceName)->first();
            if(!empty($chk_exist)){
                $a = 1;  
            }
        }else{
            $a = 0; 
        }

        if(!empty($request->memo_file)){
           
            $memoName = 'admincouncil_meeting'.'_'.$request->memo_file->getClientOriginalName();
            $pathMemo_file = $request->memo_file->storeAs('public/uploads', $memoName);
            $chk_exist = File::where('name' , $memoName)->first();
            if(!empty($chk_exist)){   
                $m2 = 2;
            }
        }else{
            $m2 = 0;
        }

        if(!empty($request->document_file)){
            $documentName = 'admincouncil_meeting'.'_'.$request->document_file->getClientOriginalName();
            $chk_exist = File::where('name' , $documentName)->first();
            if(!empty($chk_exist)){
                $d = 1;
            }
        }else{
            $d = 0;
        }

        
        if($m == 1 && $a == 1 && $m2 == 1 && $d == 1){
            return redirect()->intended(route('admin.edit.admincouncil', ['id' => $id ]))->with('error', 'Duplicate File');
        }elseif($m == 1){
            return redirect()->intended(route('admin.create.admincouncil', ['id' => $id ]))->with('error', 'Minutes File is a Duplicate File');
        }
        elseif($a == 1){
            return redirect()->intended(route('admin.create.admincouncil', ['id' => $id ]))->with('error', 'Attendance File is a Duplicate File');
        }
        elseif($m2 == 1){
            return redirect()->intended(route('admin.create.admincouncil', ['id' => $id ]))->with('error', 'Memorandum File is a Duplicate File');
        }
        elseif($d == 1){
            return redirect()->intended(route('admin.create.admincouncil', ['id' => $id ]))->with('error', 'Document File is a Duplicate File');
        }elseif($m == 0 && $a == 0 && $m2 == 0 && $d == 0){
            $stmt = $id->update($data);
            if(!$stmt){
                return redirect()->intended(route('admin.edit.admincouncil', ['id' => $id]))->with('error', 'Update Failed');
            }else{
                return redirect()->intended(route('admin.admincouncil'))->with('success', 'Update Success'); 
            }
        }elseif($m == 0){

            $attendance_file = File::where('id', $attendance_file_id)->first();
            Storage::delete($attendance_file->file_path);
            $pathAttendance_file = $request->attendance_file->storeAs('public/uploads', $attendanceName);
            File::where('id', $attendance_file_id)->update([
                'name' => $attendanceName,
                'file_path' => $pathAttendance_file,
            ]);

            $memo_file = File::where('id', $memo_file_id)->first();
            Storage::delete($memo_file->file_path);
            File::where('id', $memo_file_id)->update([
                'name' => $memoName,
                'file_path' => $pathMemo_file,
            ]);
            
            $document_file = File::where('id', $document_file_id)->first();
            Storage::delete($document_file->file_path);
            $pathDocument_file = $request->document_file->storeAs('public/uploads', $documentName);
            File::where('id', $document_file_id)->update([
                'name' => $documentName,
                'file_path' => $pathDocument_file,
            ]);

           
        }elseif($a == 0){

            $minute_file = File::where('id', $minute_file_id)->first();
            Storage::delete($minute_file->file_path);
            $pathMinute_file = $request->minute_file->storeAs('public/uploads', $minuteName);
            $stmt = File::where('id', $minute_file_id)->update([
                'name' => $minuteName,
                'file_path' => $pathMinute_file,
            ]);
            
            $memo_file = File::where('id', $memo_file_id)->first();
            Storage::delete($memo_file->file_path);
            File::where('id', $memo_file_id)->update([
                'name' => $memoName,
                'file_path' => $pathMemo_file,
            ]);
            
            $document_file = File::where('id', $document_file_id)->first();
            Storage::delete($document_file->file_path);
            $pathDocument_file = $request->document_file->storeAs('public/uploads', $documentName);
            File::where('id', $document_file_id)->update([
                'name' => $documentName,
                'file_path' => $pathDocument_file,
            ]);

            

        }elseif($m == 0){
            $minute_file = File::where('id', $minute_file_id)->first();
            Storage::delete($minute_file->file_path);
            $pathMinute_file = $request->minute_file->storeAs('public/uploads', $minuteName);
            $stmt = File::where('id', $minute_file_id)->update([
                'name' => $minuteName,
                'file_path' => $pathMinute_file,
            ]);
            
            $attendance_file = File::where('id', $attendance_file_id)->first();
            Storage::delete($attendance_file->file_path);
            $pathAttendance_file = $request->attendance_file->storeAs('public/uploads', $attendanceName);
            File::where('id', $attendance_file_id)->update([
                'name' => $attendanceName,
                'file_path' => $pathAttendance_file,
            ]);
            
            $document_file = File::where('id', $document_file_id)->first();
            Storage::delete($document_file->file_path);
            $pathDocument_file = $request->document_file->storeAs('public/uploads', $documentName);
            File::where('id', $document_file_id)->update([
                'name' => $documentName,
                'file_path' => $pathDocument_file,
            ]);
        }elseif($d == 0){
            $minute_file = File::where('id', $minute_file_id)->first();
            Storage::delete($minute_file->file_path);
            $pathMinute_file = $request->minute_file->storeAs('public/uploads', $minuteName);
            $stmt = File::where('id', $minute_file_id)->update([
                'name' => $minuteName,
                'file_path' => $pathMinute_file,
            ]);
            
            $attendance_file = File::where('id', $attendance_file_id)->first();
            Storage::delete($attendance_file->file_path);
            $pathAttendance_file = $request->attendance_file->storeAs('public/uploads', $attendanceName);
            File::where('id', $attendance_file_id)->update([
                'name' => $attendanceName,
                'file_path' => $pathAttendance_file,
            ]);

            $memo_file = File::where('id', $memo_file_id)->first();
            Storage::delete($memo_file->file_path);
            File::where('id', $memo_file_id)->update([
                'name' => $memoName,
                'file_path' => $pathMemo_file,
            ]);
        }else{

            $minute_file = File::where('id', $minute_file_id)->first();
            Storage::delete($minute_file->file_path);
            $pathMinute_file = $request->minute_file->storeAs('public/uploads', $minuteName);
            $stmt = File::where('id', $minute_file_id)->update([
                'name' => $minuteName,
                'file_path' => $pathMinute_file,
            ]);
            
            $attendance_file = File::where('id', $attendance_file_id)->first();
            Storage::delete($attendance_file->file_path);
            $pathAttendance_file = $request->attendance_file->storeAs('public/uploads', $attendanceName);
            File::where('id', $attendance_file_id)->update([
                'name' => $attendanceName,
                'file_path' => $pathAttendance_file,
            ]);

            $memo_file = File::where('id', $memo_file_id)->first();
            Storage::delete($memo_file->file_path);
            File::where('id', $memo_file_id)->update([
                'name' => $memoName,
                'file_path' => $pathMemo_file,
            ]);
            
            $document_file = File::where('id', $document_file_id)->first();
            Storage::delete($document_file->file_path);
            $pathDocument_file = $request->document_file->storeAs('public/uploads', $documentName);
            File::where('id', $document_file_id)->update([
                'name' => $documentName,
                'file_path' => $pathDocument_file,
            ]);

            
        }

        $stmt = $id->update($data);
        if(!$stmt){
            return redirect()->intended(route('admin.admin-council.edit', ['id' => $id]))->with('error', 'Update Failed');
        }else{
            return redirect()->intended(route('admin.admin-council.index'))->with('success', 'Update Success'); 
        } */

       
       
        
    }

    public function destroy(string $id){

        $data = DocumentFiles::where('meeting_details_id', $id)->get();

        foreach($data as $d){
            $file_id = $d->file_id;
            $file = File::where('id', $file_id)->first();
            Storage::delete($file->file_path);
            File::where('id', $file_id)->delete();
            DocumentFiles::where('meeting_details_id',$id)->where('file_id',$file_id)->delete();
        }

        $stmt = MeetingDetails::where('id', $id)->delete();
        if(!$stmt){
            return redirect()->intended(route('admin.admin-council.index'))->with('error', 'Delete Failed');
        }else{
            return redirect()->intended(route('admin.admin-council.index'))->with('success', 'Delete Successful!');
        }

    }

    public function download(string $id){
        $stmt = File::where('id', $id)->first();
        $data = Storage::get($stmt->file_path);
        
        $response = Response::make($data, 200);
        $response->header('Content-Type', 'application/pdf');
        return $response;

    }

    public function meetingDetails(MeetingDetails $id){
        

        $meeting_details =  MeetingDetails::where('id', $id->id)->first();
        $category = Category::where('id', $id->category_id)->first();
        $document_files = DocumentFiles::where('meeting_details_id', $id->id)->get();
        $details = Details::where('meeting_details_id', $id->id)->first();
        
        if($document_files->count()){
            foreach($document_files AS $df){
                $files[] = File::WHERE('id', $df->file_id)->get();
                
            }
            
        }else{
            $files = '';
        }
       
       
        
        return view('pages.admin.admin-council.meeting-details',[
            'meeting_details' => $meeting_details,
            'category'=> $category,
            'files' => $files,
            'details' => $details,
        ]);
      
    }
}
