<?php

namespace App\Http\Controllers;

use App\Models\MeetingDetails;
use App\Models\Superadmin\Category;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Knp\Snappy\Pdf;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //

        date_default_timezone_set('Asia/Manila');
        $currentYear = date('Y');
        $years = range($currentYear, 2000);

        $category = Category::get();
        $ac = MeetingDetails::select('meeting_details.meeting_date')
        ->join('category', 'category.id', 'meeting_details.category_id')
        ->where('category.categoryName', 'Admin Council')
        ->groupBy('meeting_date','category_id')->get();
        $act = $ac->count();

        $ad = MeetingDetails::select('meeting_details.meeting_date')
        ->join('category', 'category.id', 'meeting_details.category_id')
        ->where('category.categoryName', 'Academic Council')
        ->groupBy('meeting_date','category_id')->get();
        $adt = $ad->count();

        $pbm = MeetingDetails::select('meeting_details.meeting_date')
        ->join('category', 'category.id', 'meeting_details.category_id')
        ->where('category.categoryName', 'Preboard Meeting')
        ->groupBy('meeting_date','category_id')->get();
        $pbmm = $pbm->count();

        $rbm = MeetingDetails::select('meeting_details.meeting_date')
        ->join('category', 'category.id', 'meeting_details.category_id')
        ->where('category.categoryName', 'Regular Board Meeting')
        ->groupBy('meeting_date','category_id')->get();
        $rbmm = $rbm->count();

        $sm = MeetingDetails::select('meeting_details.meeting_date')
        ->join('category', 'category.id', 'meeting_details.category_id')
        ->where('category.categoryName', 'Special Meeting')
        ->groupBy('meeting_date','category_id')->get();
        $spm = $sm->count();

       
        return view('pages.admin.reports.index',[
            'category' => $category,
            'act' => $act,
            'adt' => $adt,
            'pbmm' => $pbmm,
            'rbmm' => $rbmm,
            'spm' => $spm,
            'current_year' => $currentYear,
            'years' => $years,
        ]);  




    }

    public function viewReports(string $id){
        
        $x = MeetingDetails::select('meeting_date')->where('category_id', $id)->groupByRaw('meeting_date')->get();
        $meetingCount = $x->count();

        $category = Category::where('id',$id)->first();
        $category_name = $category->categoryName;
        
        $currentYear = date('Y');
        $years = range($currentYear, 2000);

        

        return view('pages.admin.reports.view-reports',[
            'data' => $x,
            'years' => $years,
            'category_name' => $category_name,
        ]);

    }

    public function fetchData(Request $request){
        
        $year = $request->year;
        $month = $request->month;

        if($month == 00){
            $stmt = MeetingDetails::whereYear('created_at', $year)->get();
        }else{
            $stmt = MeetingDetails::whereYear('created_at', $year)
            ->whereMonth('create_at', $month)
            ->get();
        }

        return response()->json([
            'data' => $stmt,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function filter_year(Request $request){
        if($request->ajax()){
          
            $category = $request->category;
            $year = $request->year;
            $month = $request->month; 
            
            if($category != '' && $year == '' && $month == ''){
                $selected_category = MeetingDetails::where('category_id', $category)->groupBy('meeting_date','category_id')->get();
                $category = Category::where('id', $category)->first();
                $total_count = array([
                    'category' => $category->categoryName,
                    'count' => $selected_category->count(),
                ]);
                return response()->json([
                    'total' => $total_count,
                ]);
            }elseif($category != '' && $year != '' & $month == ''){
                $selected_category = MeetingDetails::where('category_id', $category)
                ->whereYear('meeting_date', $year)
                ->groupBy('meeting_date','category_id')
                ->get();
                
                $category = Category::where('id', $category)->first();
                
                $total_count = array([
                    'category' => $category->categoryName,
                    'count' => $selected_category->count(),
                ]);
    
                return response()->json([
                   
                    'total' => $total_count,
                ]);
            }elseif($category != '' && $year != '' & $month != ''){
                $selected_category = MeetingDetails::where('category_id', $category)
                ->whereYear('meeting_date', $year)
                ->whereMonth('meeting_date', $month)
                ->groupBy('meeting_date','category_id')
                ->get();
                
                $category = Category::where('id', $category)->first();
                
                $total_count = array([
                    'category' => $category->categoryName,
                    'count' => $selected_category->count(),
                ]);
    
                return response()->json([
                    'total' => $total_count,
                ]);
            }

          
        }
    }

    public function print_pdf(Request $request){
        
        $category = $request->category;
        $year = $request->year;
        $month = $request->month;

        if($category == '' && $year == '' && $month == ''){

            $category = Category::get();
            $ac = MeetingDetails::select('meeting_details.meeting_date')
            ->join('category', 'category.id', 'meeting_details.category_id')
            ->where('category.categoryName', 'Admin Council')
            ->groupBy('meeting_date','category_id')->get();
            $act = $ac->count();

    
            $ad = MeetingDetails::join('category', 'category.id', 'meeting_details.category_id')
            ->where('category.categoryName','Academic Council')
            ->groupBy('meeting_date','category_id')->get();
            $adt = $ad->count();

            $pbm = MeetingDetails::select('meeting_details.meeting_date')
            ->join('category', 'category.id', 'meeting_details.category_id')
            ->where('category.categoryName', 'Preboard Meeting')
            ->groupBy('meeting_date','category_id')->get();
            $pbmm = $pbm->count();

            $rbm = MeetingDetails::select('meeting_details.meeting_date')
            ->join('category', 'category.id', 'meeting_details.category_id')
            ->where('category.categoryName', 'Regular Board Meeting')
            ->groupBy('meeting_date','category_id')->get();
            $rbmm = $rbm->count();

            $sm = MeetingDetails::select('meeting_details.meeting_date')
            ->join('category', 'category.id', 'meeting_details.category_id')
            ->where('category.categoryName', 'Special Meeting')
            ->groupBy('meeting_date','category_id')->get();
            $spm = $sm->count();

            $total = MeetingDetails::groupBy('meeting_date', 'category_id')
            ->get();
            $fetch_total = $total->count();

            $pdf= SnappyPdf::loadView('pdf.example',[
                'action' => '1',
                'category' => $category,
                'act' => $act,
                'adt' => $adt,
                'pbmm' => $pbmm,
                'rbmm' => $rbmm,
                'spm' => $spm,
                'total' => $fetch_total,
            ]);

        }elseif($category != '' && $year == '' && $month == ''){
            
            $selected_category = MeetingDetails::where('category_id', $category)->groupBy('meeting_date','category_id')->get();
            $categories = Category::where('id', $category)->first();
          
            $total_count = array([
                'category' => $categories->categoryName,
                'count' => $selected_category->count(),
            ]);

            $pdf = SnappyPdf::loadView('pdf.example',[
                'action' => '2',
                'total' => $total_count,
            ]);

        }elseif($category != '' && $year != '' && $month == ''){
            $selected_category = MeetingDetails::where('category_id', $category)
            ->whereYear('meeting_date', $year)
            ->groupBy('meeting_date','category_id')
            ->get();
                
            $category = Category::where('id', $category)->first();
                
            $total_count = array([
                'category' => $category->categoryName,
                'year' => $year,
                'count' => $selected_category->count(),
            ]);
            $pdf = SnappyPdf::loadView('pdf.example',[
                'action' => '3',
                'total' => $total_count,
            ]);
        }elseif($category != '' && $year != '' && $month != ''){

            $selected_category = MeetingDetails::where('category_id', $category)
            ->whereYear('meeting_date', $year)
            ->whereMonth('meeting_date', $month)
            ->groupBy('meeting_date','category_id')
            ->get();
                
            $category = Category::where('id', $category)->first();
                
            $total_count = array([
                'category' => $category->categoryName,
                'year' => $year,
                'count' => $selected_category->count(),
            ]);
    
            $pdf = SnappyPdf::loadView('pdf.example',[
                'action' => '3',
                'total' => $total_count,
            ]);
        }
        
        return $pdf->stream('reports.pdf');
        return view('pdf.example');
    }
}
