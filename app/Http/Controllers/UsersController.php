<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserAccountRequest;
use App\Http\Requests\UserAccountRequestUpdate;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $id = auth()->user()->id;
        $userInformation = User::where('id', $id)->first();

        return view('pages.admin.users.index',[
            'userInformation' => $userInformation,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserAccountRequest $request, User $user_account)
    {
        
        if(Hash::check($request->validated('old_password'), $user_account->password)){
            $username = $request->validated('username');
            $new_password = Hash::make($request->validated('new_password'));
            
            $stmt = User::where('id', $user_account->id)
                ->update([
                    'username' => $username, 
                    'password' => $new_password
                ]);
            if(!$stmt){
                return redirect()->intended(route('admin.user-account.index'))->with('error', 'User Update Error');
            }else{
                return redirect()->intended(route('logout'));
            }
        }else{
            return redirect()->intended(route('admin.user-account.index'))->with('error', 'Old Password Doesnt Match');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
