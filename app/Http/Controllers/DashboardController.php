<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\MeetingDetails;
use App\Models\Superadmin\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    public function index(){

        
        $admin = MeetingDetails::JOIN('category', 'meeting_details.category_id', '=', 'category.id')->where('categoryName', 'Admin Council')->groupBy('meeting_date')->get();
        $adminTotal = $admin->count();
        
        
        $academic = MeetingDetails::JOIN('category', 'meeting_details.category_id', '=', 'category.id')->where('categoryName', 'Academic Council')->groupBy('meeting_date')->get();
        $academicTotal = $academic->count();
        
        $preboard = MeetingDetails::JOIN('category', 'meeting_details.category_id', '=', 'category.id')->where('categoryName', 'Preboard Meeting')->groupBy('meeting_date')->get();
        $preboardTotal = $preboard->count();
        
        $regularBoard = MeetingDetails::JOIN('category', 'meeting_details.category_id', '=', 'category.id')->where('categoryName', 'Regular Board Meeting')->groupBy('meeting_date')->get();
        $regularBoardTotal = $regularBoard->count();

        $special = MeetingDetails::JOIN('category', 'meeting_details.category_id', '=', 'category.id')->where('categoryName', 'Regular Board Meeting')->groupBy('meeting_date')->get();
        $specialTotal = $special->count();

        return view('pages.admin.dashboard', [
            'adminTotal' => $adminTotal,
            'academicTotal' => $academicTotal,
            'preboardTotal' => $preboardTotal,
            'regularBoardTotal' => $regularBoardTotal,
            'specialTotal' => $specialTotal, 
        ]);
    }

    public function getData(){
        $admin = MeetingDetails::JOIN('category', 'meeting_details.category_id', '=', 'category.id')->where('categoryName', 'Admin Council')->groupBy('meeting_date')->get();
        $adminTotal = $admin->count();
        
        $academic = MeetingDetails::JOIN('category', 'meeting_details.category_id', '=', 'category.id')->where('categoryName', 'Academic Council')->groupBy('meeting_date')->get();
        $academicTotal = $academic->count();
        
        $preboard = MeetingDetails::JOIN('category', 'meeting_details.category_id', '=', 'category.id')->where('categoryName', 'Preboard Meeting')->groupBy('meeting_date')->get();
        $preboardTotal = $preboard->count();
        
        $regularBoard = MeetingDetails::JOIN('category', 'meeting_details.category_id', '=', 'category.id')->where('categoryName', 'Regular Board Meeting')->groupBy('meeting_date')->get();
        $regularBoardTotal = $regularBoard->count();

        $special = MeetingDetails::JOIN('category', 'meeting_details.category_id', '=', 'category.id')->where('categoryName', 'Regular Board Meeting')->groupBy('meeting_date')->get();
        $specialTotal = $special->count();

        return response()->json([
            'admin' => $adminTotal,
            'academic' => $academicTotal,
            'preboard' => $preboardTotal,
            'regularBoard' => $regularBoardTotal,
            'special' => $specialTotal,
        ]);

    }


    public function store(Request $request){    
        $data = $request->validate([
            'category_id' => 'required',
            'doc_type' => 'required',
            'doc_description' => 'required',
            'agenda' => 'required',
            'resolution_number' => 'required',
            'resolution_subject' => 'required',
            'minute_file' => 'mimes:csv,txt,xlx,xls,pdf',
            'attendance_file' => 'mimes:csv,txt,xlx,xls,pdf',
            'memo_number' => 'required',
            'memo_subject' => 'required',
            'memo_file' => 'mimes:csv,txt,xlx,xls,pdf',
            'quarter' => 'required',
            'document_file' => 'mimes:csv,txt,xlx,xls,pdf',
            'document_description' => 'required',
            'remarks' => 'required',
        ]);


        $stmt = MeetingDetails::create($data);
        $lid = $stmt->id;

        $minuteName = uniqid() . '_' . $request->minute_file->getClientOriginalName();
        $pathMinute_file = $request->minute_file->storeAs('public/uploads', $minuteName);

        $attendanceName = uniqid() . '_' . $request->attendance_file->getClientOriginalName();
        $pathAttendance_file = $request->attendance_file->storeAs('public/uploads', $attendanceName);

        $memoName = uniqid() . '_' . $request->memo_file->getClientOriginalName();
        $pathMemo_file = $request->memo_file->storeAs('public/uploads', $memoName);

        $documentName = uniqid() . '_' . $request->document_file->getClientOriginalName();
        $pathDocument_file = $request->document_file->storeAs('public/uploads', $documentName);

        $minutes = File::create([
            'name' => $minuteName,
            'file_path' => $pathMinute_file,
        ]);
        $attendance = File::create([
            'name' => $attendanceName,
            'file_path' => $pathAttendance_file,
        ]);
        $memo = File::create([
            'name' => $memoName,
            'file_path' => $pathMemo_file,
        ]);
        $document = File::create([
            'name' => $documentName,
            'file_path' => $pathDocument_file,
        ]);
        

        $min_id = $minutes->id;
        $attendance_id = $attendance->id;
        $memo_id = $memo->id;
        $document_id = $document->id;

        $updateStmt = MeetingDetails::where('id', $lid)->update([
            'minute_file_id' => $min_id,
            'attendance_file_id' => $attendance_id,
            'memo_file_id' => $memo_id,
            'document_file_id' => $document_id,
        ]);

        $category = Category::where('id', $request->category_id)->first();
        $category_name = $category->category_name;

        
        if(!$stmt AND !$updateStmt){
            return redirect()->intended(route('admin.dashboard'))->with('error', 'Saving Failed');
        }else{
            if($category_name === 'Admin Council'){
                return redirect()->intended(route('admin.admincouncil'))->with('success', 'Saving Successful');
            }elseif($category_name === 'Academic Council'){
                return redirect()->intended(route('admin.academiccouncil'))->with('success', 'Saving Successful');
            }elseif($category_name === 'Preboard Meeting'){
                return redirect()->intended(route('admin.preboardmeeting'))->with('success', 'Saving Successful');
            }elseif($category_name === 'Regular Board Meeting'){
                return redirect()->intended(route('admin.regularboardmeeting'))->with('success', 'Saving Successful');
            }elseif($category_name === 'Special Meeting'){
                return redirect()->intended(route('admin.specialmeeting'))->with('success', 'Saving Successful');
            }else{
                return redirect()->intended(route('admin.dashboard'))->with('success', 'Saving Successful');
            }
        }



    }

    public function edit(meetingDetails $id){
        $category = $id->category_id;
        $category_details = Category::where('id', $category)->first();
        $category = Category::all();
        

        return view('pages.admin.updateMeeting',[
            'meeting_details' => $id, 
            'category' => $category,
            'category_details' => $category_details,
        ]);
    }

    public function update($details, Request $request){
    
        $data = $request->validate([
            'category_id' => 'required',
            'doc_type' => 'required',
            'doc_description' => 'required',
            'agenda' => 'required',
            'resolution_number' => 'required',
            'resolution_subject' => 'required',
            'minute_file' => 'mimes:csv,txt,xlx,xls,pdf|max:2048',
            'attendance_file' => 'mimes:csv,txt,xlx,xls,pdf|max:2048',
            'memo_number' => 'required',
            'memo_subject' => 'required',
            'memo_file' => 'mimes:csv,txt,xlx,xls,pdf|max:2048',
            'quarter' => 'required',
            'document_file' => 'mimes:csv,txt,xlx,xls,pdf|max:2048',
            'document_description' => 'required',
            'remarks' => 'required',
        ]);


        $stmt = MeetingDetails::find($details);
        $stmt = $stmt->update($data);

        if(!empty($request->minute_file)){
            $minute = File::where('id', $request->minute_file)->first();
            Storage::delete($minute->file_path);
            $minuteName = uniqid() . '_' . $request->minute_file->getClientOriginalName();
            $pathMinute_file = $request->minute_file->storeAs('public/uploads', $minuteName);
            File::where('id', $request->minute_file)->update(['name' => $minuteName, 'file_path' => $pathMinute_file]);
        }

        if(!empty($request->attendance_file)){
            $attendance = File::where('id', $request->attendance_file)->first();
            Storage::delete($attendance->file_path);
            $attendanceName = uniqid() . '_' . $request->attendance_file->getClientOriginalName();
            $pathAttendance_file = $request->attendance_file->storeAs('public/uploads', $attendanceName);
            File::where('id', $request->attendance_file)->update(['name' => $attendanceName , 'file_path' => $pathAttendance_file]);
        }

        if(!empty($request->memo_file)){
            $memo = File::where('id', $request->memo_file)->first();
            Storage::delete($memo->file_path);
            $memoName = uniqid() . '_' . $request->memo_file->getClientOriginalName();
            $pathMemo_file = $request->memo_file->storeAs('public/uploads', $memoName);
            File::where('id', $request->memo_file)->update(['name' => $memoName, 'file_path' => $pathMemo_file]);
        }

        if(!empty($request->document_file)){
            $document = File::where('id', $request->document_file)->first();
            Storage::delete($document->file_path);
            $documentName = uniqid() . '_' . $request->document_file->getClientOriginalName();
            $pathDocument_file = $request->document_file->storeAs('public/uploads', $documentName);
            File::where('id', $request->document_file)->update(['name'=> $documentName, 'file_path' => $pathDocument_file]);
        }



        if(!$stmt){
            echo 1;
        }else{
            echo 2;
        }

        /* if(empty($request->minute_file)){

        }else{
            $minute_file_id = $request->minute_file_id;
            $minute_file = File::where('id', $minute_file_id)->first();
         
            Storage::delete($minute_file->file_path);
            $minuteName = uniqid() . '_' . $request->minute_file->getClientOriginalName();
            $pathMinute_file = $request->minute_file->storeAs('public/uploads', $minuteName);

            File::where('id', $minute_file_id)->update([
                'name' => $minuteName,
                'file_path' => $pathMinute_file,
            ]);
            
        } */




    }

  
}
