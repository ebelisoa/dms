<?php

namespace App\Http\Controllers;

use App\Models\DocumentFiles;
use App\Models\File;
use App\Models\MeetingDetails;
use App\Models\Superadmin\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use League\CommonMark\Node\Block\Document;

class FilesController extends Controller
{
    //
    public function index(MeetingDetails $meeting_details){
      /*  dd($meeting_details); */
        $document_files = DocumentFiles::where('meeting_details_id', $meeting_details->id)->get();
        $category = MeetingDetails::join('category', 'category.id','=','meeting_details.category_id')
        ->where('meeting_details.id', $meeting_details->id)->first('category.categoryName');

        if($document_files->count()){
            foreach($document_files AS $df){
                $files[] = File::WHERE('id', $df->file_id)->get();
                
            }
            
        }else{
            $files = [];
        }

       /*  dd($files); */
        return view('pages.admin.files.index',[
            'files' => $files,
            'meeting_details' => $meeting_details,
            'category' => $category->categoryName,
        ]);
    }

    public function add_files(string $id){
    
        return view('pages.admin.files.add-files',[
            'meeting_details_id' => $id,
          
        ]);
    }

    public function store(Request $request){
        $data = MeetingDetails::join('category', 'category.id','=','meeting_details.category_id')
            ->where('meeting_details.id', $request->id)->first();
            
        if($data->categoryName == 'Admin Council'){
            $fileNamePrefix = 'admincouncil_'.$data->document_type;
        }elseif($data->categoryName == 'Academic Council'){
            $fileNamePrefix = 'academiccouncil_'.$data->document_type;
        }elseif($data->categoryName == 'Preboard Meeting'){
            $fileNamePrefix = 'preboard_'.$data->document_type;
        }elseif($data->categoryName == 'Regular Board Meeting'){
            $fileNamePrefix = 'regularboard_'.$data->document_type;
        }elseif($data->categoryName == 'Special Meeting'){
            $fileNamePrefix = 'specialmeeting_'.$data->document_type;
        }

        foreach($request->file AS $files){
            $agenda_file_name = $fileNamePrefix.'_'.$files->getClientOriginalName();
            $path_file = $files->storeAs('public/uploads', $agenda_file_name);
            $files = File::create([
                'name' => $agenda_file_name,
                'file_path' => $path_file,
            ]);

            $files_id[] = $files->id;
        }

        foreach($files_id as $id){
            DocumentFiles::create([
                'meeting_details_id' => $request->id,
                'file_id' => $id,
            ]);
        }

        return redirect()->route('admin.files.index', ['meeting_details' => $request->id]);
        

    }

    public function download(string $id){
        
        $stmt = File::where('id', $id)->first();
        $data = Storage::get($stmt->file_path);
        
        $response = Response::make($data, 200);
        $response->header('Content-Type', 'application/pdf');
        return $response;
    }

    public function destroy(string $id){
        /* dd($id); */
        $file = File::where('id', $id)->first();
        Storage::delete($file->file_path);
        File::where('id', $id)->delete();

        $document_files = DocumentFiles::where('file_id', $id)->first();
        $meeting_details_id = $document_files->meeting_details_id;
        DocumentFiles::where('file_id',$id)->delete();

        return redirect()->route('admin.files.index', ['meeting_details' => $meeting_details_id])->with('success', 'Delete Successful');
    }
}
