<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Superadmin\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $category = Category::all();
        return view('pages.superadmin.manageCategory', [
            'category' => $category,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('pages.superadmin.createCategory');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'categoryName' => 'required|unique:category,categoryName',
        ]);
        $info = $request->only('categoryName');

        $stmt = Category::create($info);
        if(!$stmt){
            return redirect()->intended(route('superadmin.create.category'))->with('error', 'Saving Failed');
        }else{
            return redirect()->intended(route('superadmin.index.category'))->with('success', 'Data is saved successfully');
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    
    public function fetch($category_id=0){
        $catData['data'] = Category::where('id', $category_id)->get();
        return response()->json($catData);
    }
}
