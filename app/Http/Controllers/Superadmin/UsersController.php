<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $data = User::all();
        return view('pages.superadmin.users', [
            'data' => $data,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('pages.superadmin.create_users');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $data = $request->validate([
            'fullname' => 'required',
            'username' => 'required|unique:users,username',
            'password' => 'required',
        ]);

        $password = Hash::make($request->password);
        $stmt = User::create([
            'fullname'=>$request->fullname,
            'username' => $request->username,
            'password' => $password,
        ]);
        if(!$stmt){
            return redirect()->intended(route('superadmin.create.users'))->with('error', 'Something Went Wrong');
        }else{
            return redirect()->intended(route('superadmin.index.users'))->with('success', 'Something Went Wrong');
        }
        
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $data = User::where('id', $id)->first();
        return view('pages.superadmin.update_users',[
            'data' => $data,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
