<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Superadmin\DocumentTypeRequest;
use App\Models\Superadmin\DocumentTypes;
use Illuminate\Http\Request;

class DocumentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $documenttype = DocumentTypes::all();

        return view('pages.superadmin.manageDocumentType', [
            'documenttype' => $documenttype,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('pages.superadmin.createDocumentType');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(DocumentTypeRequest $request)
    {
        //

        $data = $request->validated();
        $stmt = DocumentTypes::create($data);
        if(!$stmt){
           return redirect()->intended(route('superadmin.index.documenttype'))->with('error', 'Saving Error');
        }
        return redirect()->intended(route('superadmin.index.documenttype'))->with('success', 'Saving Successfull');



    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DocumentTypes $id)
    {
        //

        return view('pages.superadmin.updateDocumentType',[
            'documenttype' => $id,
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(DocumentTypeRequest $request, DocumentTypes $id)
    {
        //
        $data = $request->validated();
        $stmt = $id->update($data);
        if(!$stmt){
            return redirect()->intended(route('superadmin.index.documenttype'))->with('error', 'Update Failed');
        }
        return redirect()->intended(route('superadmin.index.documenttype'))->with('success', 'Update Successfull');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
