<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticationRequest;
use App\Models\LoginTrails;
use App\Models\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
    public function index(){

        if(auth()->check()){
            if(auth()->user()->role == 'superadmin'){
                return redirect()->route('superadmin.index.dashboard');
            }

            if(auth()->user()->role == 'admin'){
                return redirect()->route('admin.dashboard');
            }
        }
        return redirect()->intended(route('login'));
    }



    public function login(AuthenticationRequest $request){
        if(auth()->attempt($request->validated())){
           
            $request->session()->regenerate();
            if(auth()->user()->role == 'admin'){
                return redirect()->intended(route('admin.dashboard'));
            }

            if(auth()->user()->role == 'superadmin'){
                return redirect()->intended(route('superadmin.index.dashboard'));
            }
        }
        return redirect()->intended(route('index'))->with('error', 'User Credential Error');
    }

    public function logout(){
        Auth::logout();
        return redirect()->intended(route('index'));
    }
}
