<?php

namespace App\Http\Controllers;

use App\Http\Requests\AcademicCouncilRequestStore;
use App\Http\Requests\AcademicCouncilRequestUpdate;
use App\Models\Details;
use App\Models\DocumentFiles;
use App\Models\File;
use App\Models\MeetingDetails;
use App\Models\Superadmin\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use PDO;

class AcademicCouncilController extends Controller
{
    public function index(){

        $data = MeetingDetails::join('category', 'meeting_details.category_id', '=', 'category.id')
        ->where('category.categoryName', 'Academic Council')
        ->get('meeting_details.*');

        $category = Category::where('categoryName', 'Academic Council')->first('id');
        return view('pages.admin.academic-council.index',[
            'meeting_details' => $data,
            'category' => $category, 
        ]);
    }

    public function create(string $id){
        return view('pages.admin.academic-council.create', [
            'category' => $id,
        ]);
    }

    public function store(AcademicCouncilRequestStore $request){
      
        $data = $request->validated();
        $stmt = MeetingDetails::create($data);
        $lid = $stmt->id;

        //recieve the file 
        if($request->document_type == 'agenda' && $request->agenda != null){
            $agenda_file_id = array();
            
            foreach($request->agenda AS $f){
                $agenda_file_name = 'academiccouncil_agenda'.'_'.$f->getClientOriginalName();
                $path_file = $f->storeAs('public/uploads', $agenda_file_name);
                $files = File::create([
                    'name' => $agenda_file_name,
                    'file_path' => $path_file,
                ]);
                
                $agenda_file_id[] = $files->id;
             
            }

            foreach($agenda_file_id as $id){
               DocumentFiles::create([
                        'meeting_details_id' => $lid,
                        'file_id' => $id,
                ]);
            }
        }elseif($request->document_type == 'resolution' && $request->resolution_file != null){

            $resolution_file_id = array();

            if(empty($request->resolution_number) AND empty($request->resolution_subject)){
    
            }else{

                foreach($request->resolution_file AS $f){
                    $resolution_file_name = 'academiccouncil_resolution'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $resolution_file_name);
                    $files = File::create([
                        'name' => $resolution_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $resolution_file_id[] = $files->id;
                 
                }
    
                foreach($resolution_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }

                Details::create([
                    'meeting_details_id' => $lid,
                    'no' => $request->resolution_number,
                    'subject' => $request->resolution_subject,
                ]);
            }
        }elseif($request->document_type == 'minutes' && $request->minutes_file != null){
            $minutes_file_id = array();
            
            foreach($request->minutes_file AS $f){
                $minutes_file_name = 'academiccouncil_minutes'.'_'.$f->getClientOriginalName();
                $path_file = $f->storeAs('public/uploads', $minutes_file_name);
                $files = File::create([
                    'name' => $minutes_file_name,
                    'file_path' => $path_file,
                ]);
                
                $minutes_file_id[] = $files->id;
             
            }

            foreach($minutes_file_id as $id){
               DocumentFiles::create([
                        'meeting_details_id' => $lid,
                        'file_id' => $id,
                ]);
            }
        }elseif($request->document_type == 'attendance' && $request->attendance_file){
            $attendance_file_id = array();
            
            foreach($request->attendance_file AS $f){
                $attendance_file_name = 'academiccouncil_attendance'.'_'.$f->getClientOriginalName();
                $path_file = $f->storeAs('public/uploads', $attendance_file_name);
                $files = File::create([
                    'name' => $attendance_file_name,
                    'file_path' => $path_file,
                ]);
                
                $attendance_file_id[] = $files->id;
             
            }

            foreach($attendance_file_id as $id){
               DocumentFiles::create([
                        'meeting_details_id' => $lid,
                        'file_id' => $id,
                ]);
            }
        }elseif($request->document_type == 'memo' AND $request->memo_file != null){
            $memo_file_id = array();

            if(empty($request->memo_number) AND empty($request->memo_subject)){
    
            }else{

                foreach($request->memo_file AS $f){
                    $memo_file_name = 'academiccouncil_documentation'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $memo_file_name);
                    $files = File::create([
                        'name' => $memo_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $memo_file_id[] = $files->id;
                 
                }
    
                foreach($memo_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }

                Details::create([
                    'meeting_details_id' => $lid,
                    'no' => $request->memo_number,
                    'subject' => $request->memo_subject,
                ]);
            }
        }elseif($request->document_type == 'documentation'){

            if(empty($request->link)){
                foreach($request->image_file AS $f){
                    $image_file_name = 'academiccouncil_documentation'.'_'.$f->getClientOriginalName();
                    $path_file = $f->storeAs('public/uploads', $image_file_name);
                    $files = File::create([
                        'name' => $image_file_name,
                        'file_path' => $path_file,
                    ]);
                    
                    $image_file_id[] = $files->id;
                 
                }
    
                foreach($image_file_id as $id){
                   DocumentFiles::create([
                            'meeting_details_id' => $lid,
                            'file_id' => $id,
                    ]);
                }
            }else{
                Details::create([
                    'meeting_details_id' => $lid,
                    'subject' => $request->link,
                ]);
            }

        }else{

        }

        if(!$stmt){
            return redirect()->intended(route('admin.academic-council.index'))->with('error', 'Saving Failed');
        }
        return redirect()->intended(route('admin.academic-council.index'))->with('success', 'Saving Successful');
    }

    public function edit(string $id){
    
        $meeting_details = MeetingDetails::where('id', $id)->first();
        $additional_details = Details::where('meeting_details_id', $id)->first();
        
        $file_details = DocumentFiles::join('files', 'files.id', 'document_files.file_id')->where('document_files.meeting_details_id', $id)->get([
            'document_files.id AS dfId',
            'files.id AS fId',
            'files.name',
        ]);
        

        if(empty($additional_details)){
            $no = '';
            $subject = '';
        }else{
            $no = $additional_details->no;
            $subject = $additional_details->subject;
        } 

        $category = Category::where('categoryName', 'Academic Council')->first('id');
        
        return view('pages.admin.academic-council.edit',[
            'meeting_details' => $meeting_details,
            'no' => $no,
            'subject' =>$subject,
            'category' => $category,
            'file_details' => $file_details,
        ]);

    }

    public function update(AcademicCouncilRequestUpdate $requests){
        
        
        $meeting_details_id = $requests->validated('meeting_details_id');

        $update_meeting_details = MeetingDetails::where('id', $meeting_details_id)->update([
            'meeting_date' => $requests->validated('meeting_date'),
            'meeting_time' => $requests->validated('meeting_time'),
            'document_type' => $requests->validated('document_type'),
            'document_description' => $requests->validated('document_description'),
            'remarks' => $requests->validated('remarks'),
        ]);

        if($requests->validated('document_type') == 'resolution'){
            Details::where('meeting_details_id', $meeting_details_id)->update([
                'no' => $requests->resolution_number,
                'subject' => $requests->resolution_subject,
            ]); 
        }elseif($requests->validated('document_type') == 'memo'){
            Details::where('meeting_details_id', $meeting_details_id)->update([
                'no' => $requests->memo_number,
                'subject' => $requests->memo_subject,
            ]); 
        }elseif($requests->validated('document_type') == 'documentation'){
            Details::where('meeting_details_id', $meeting_details_id)->update([
                'subject' => $requests->link,
            ]); 
        }else{

        }

        if(!$update_meeting_details){
            return redirect()->intended(route('admin.academiccouncil'))->with('failed', 'Update Failed!, something wrong happened!');
        }else{
            return redirect()->intended(route('admin.academic-council.index'))->with('success', 'Update Sucecssful!');
        }
    }

    public function destroy(string $id){
        
        $data = DocumentFiles::where('meeting_details_id', $id)->get();

        foreach($data as $d){
            $file_id = $d->file_id;
            $file = File::where('id', $file_id)->first();
            Storage::delete($file->file_path);
            File::where('id', $file_id)->delete();
            DocumentFiles::where('meeting_details_id',$id)->where('file_id',$file_id)->delete();
        }

        $stmt = MeetingDetails::where('id', $id)->delete();
        if(!$stmt){
            return redirect()->intended(route('admin.academic-council.index'))->with('error', 'Delete Failed');
        }else{
            return redirect()->intended(route('admin.academic-council.index'))->with('success', 'Delete Successful!');
        }

    }

    public function download(string $id){
        $stmt = File::where('id', $id)->first();
        $data = Storage::get($stmt->file_path);
        
        $response = Response::make($data, 200);
        $response->header('Content-Type', 'application/pdf');
        return $response;
        
    }

    public function meeting(MeetingDetails $id){
     

        $meeting_details =  MeetingDetails::where('id', $id->id)->first();
        $category = Category::where('id', $id->category_id)->first();
        $document_files = DocumentFiles::where('meeting_details_id', $id->id)->get();
        $details = Details::where('meeting_details_id', $id->id)->first();
        
        if($document_files->count()){
            foreach($document_files AS $df){
                $files[] = File::WHERE('id', $df->file_id)->get();
            }
            
        }else{
            $files = '';
        }
       
        
        return view('pages.admin.academic-council.meeting-details',[
            'meeting_details' => $meeting_details,
            'category'=> $category,
            'files' => $files,
            'details' => $details,
        ]);
      
    }

    public function remove(request $request){
        dd($request);
    }

}
