<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SpecialMeetingRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'doc_type' => 'required',
            'doc_description' => 'required',
            'agenda' => 'required',
            'resolution_number' => 'required',
            'resolution_subject' => 'required',
            'minute_file.*' => 'mimes:pdf,docx,doc,pptx,xlsx,txt, xls',
            'attendance_file.*' => 'mimes:pdf,docx,doc,pptx,xlsx,txt, xls',
            'memo_number' => 'required',
            'memo_subject' => 'required',
            'memo_file.*' => 'mimes:pdf,docx,doc,pptx,xlsx,txt,xls',
            'quarter' => 'required',
            'document_file.*' => 'mimes:pdf,docx,doc,pptx,xlsx,txt,xls',
            'document_description' => 'required',
            'remarks' => 'required',
        ];
    }
}
