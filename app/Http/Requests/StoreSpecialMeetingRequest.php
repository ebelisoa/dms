<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSpecialMeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        if($this->document_type == 'agenda'){
            return [
                
                'category_id' => 'required',
                'meeting_date' => 'required',
                'meeting_time' => 'required',
                'document_type' => 'required',
                'document_description' => 'required',
                'agenda' => 'nullable',
                'resolution_number' => 'nullable',
                'resolution_subject' => 'nullable',
                'memo_number' => 'nullable', 
                'memo_subject' => 'nullable',
                'link' => 'nullable',
                'quarter' => 'nullable',
                'remarks' => 'required',
            ];
        }elseif($this->document_type == 'resolution'){

            return [
                
                'category_id' => 'required',
                'meeting_date' => 'required',
                'meeting_time' => 'required',
                'document_type' => 'required',
                'document_description' => 'required',
                'resolution_number' => 'required',
                'resolution_subject' => 'required',
                'resolution_file' => 'nullable',
                'quarter' => 'nullable',
                'remarks' => 'required',
            ];

        }elseif($this->document_type == 'minutes'){
            return [
               
                'category_id' => 'required',
                'meeting_date' => 'required',
                'meeting_time' => 'required',
                'document_type' => 'required',
                'document_description' => 'required',
                'minute_file' => 'nullable',
                'quarter' => 'nullable',
                'remarks' => 'required',
                
            ];

        }elseif($this->document_type == 'attendance'){
            return [
               
                'category_id' => 'required',
                'meeting_date' => 'required',
                'meeting_time' => 'required',
                'document_type' => 'required',
                'document_description' => 'required',
                'attendance_file' => 'nullable',
                'quarter' => 'nullable',
                'remarks' => 'required',
                
            ];
        }elseif($this->document_type == 'memo'){
            return [
               
                'category_id' => 'required',
                'meeting_date' => 'required',
                'meeting_time' => 'required',
                'document_type' => 'required',
                'document_description' => 'required',
                'memo_number' => 'required',
                'memo_subject' => 'required',
                'memo_file' => 'nullable',
                'quarter' => 'nullable',
                'remarks' => 'required',
                
            ];
        }elseif($this->document_type == 'documentation'){
            return [
               
                'category_id' => 'required',
                'meeting_date' => 'required',
                'meeting_time' => 'required',
                'document_type' => 'required',
                'document_description' => 'required',
                'document_file' => 'nullable',
                'link' => 'nullable',
                'quarter' => 'nullable',
                'remarks' => 'required',
                
            ];
        }else{
            return [
               
                'category_id' => 'required',
                'meeting_date' => 'required',
                'meeting_time' => 'required',
                'document_type' => 'required',
                'document_description' => 'required',
                'agenda' => 'nullable',
                'resolution_number' => 'required',
                'resolution_subject' => 'required',
                'minute_file' => 'nullable',
                'attendance_file' => 'nullable',
                'memo_number' => 'required',
                'memo_subject' => 'required',
                'memo_file' => 'nullable',
                'document_file' => 'nullable',
                'quarter' => 'nullable',
                'remarks' => 'required',
                
            ];
        }
    }
}
