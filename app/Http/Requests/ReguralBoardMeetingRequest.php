<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReguralBoardMeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'category_id' => 'required',
            'doc_type' => 'required',
            'doc_description' => 'required',
            'agenda' => 'required',
            'resolution_number' => 'required',
            'resolution_subject' => 'required',
            'minute_file' => 'mimes:csv,txt,xlx,xls,pdf,docx,xlxs',
            'attendance_file' => 'mimes:csv,txt,xlx,xls,pdf,docx,xlxs',
            'memo_number' => 'required',
            'memo_subject' => 'required',
            'memo_file' => 'mimes:csv,txt,xlx,xls,pdf,docx,xlxs',
            'quarter' => 'required',
            'document_file' => 'mimes:csv,txt,xlx,xls,pdf,docx,xlxs',
            'document_description' => 'required',
            'remarks' => 'required',
        ];
    }
}
