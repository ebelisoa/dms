<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReguralBoardMeetingRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'doc_type' => 'required',
            'doc_description' => 'required',
            'agenda' => 'required',
            'resolution_number' => 'required',
            'resolution_subject' => 'required',
            'minute_file' => 'mimes:xlsx, csv, xls, xlx, doc,docx, pdf, txt, pptx, xlsx,doc',
            'attendance_file' => 'mimes:xlsx, csv, xls, xlx, doc,docx, pdf, txt, pptx, xlsx,doc',
            'memo_number' => 'required',
            'memo_subject' => 'required',
            'memo_file' => 'mimes:xlsx, csv, xls, xlx, doc,docx, pdf, txt, pptx, xlsx,doc',
            'quarter' => 'required',
            'document_file' => 'mimes:xlsx, csv, xls, xlx, doc,docx, pdf, txt, pptx, xlsx,doc',
            'document_description' => 'required',
            'remarks' => 'required',
        ];
    }
}
