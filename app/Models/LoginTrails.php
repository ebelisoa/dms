<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoginTrails extends Model
{
    use HasFactory;
    protected $table = 'loginaudit';
    protected $fillable = [
        'user_id',
        'action',
    ];
}
