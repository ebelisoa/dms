<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentFiles extends Model
{
    use HasFactory;
    protected $table = 'document_files';
    
    protected $fillable = [
        'meeting_details_id',
        'file_id',
    ];

}
