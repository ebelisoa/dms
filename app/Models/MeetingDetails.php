<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MeetingDetails extends Model
{
    use HasFactory;
    protected $table = 'meeting_details';
    protected $fillable = [

        'meeting_date',
        'meeting_time',
        'category_id',
        'document_type',
        'document_description',
        'quarter',
        'remarks',
    ];

    public function categoryFile(){
        return $this->belongsTo(File::class, 'category_document_file_id');
    }

    public function minuteFile(){
        return $this->belongsTo(File::class, 'minute_file_id');
    }
    public function attendanceFile(){
        return $this->belongsTo(File::class, 'attendance_file_id');
    }
    public function memoFile(){
        return $this->belongsTo(File::class, 'memo_file_id');
    }
    public function documentFile(){
        return $this->belongsTo(File::class, 'document_file_id');
    }


}
