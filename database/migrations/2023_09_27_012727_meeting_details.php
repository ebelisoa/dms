<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('meeting_details', function(Blueprint $table){
            $table->id();
            $table->date('meeting_date');
            $table->time('meeting_time');
            $table->integer('category_id');
            $table->string('document_type');
            $table->text('document_description');
            $table->integer('quarter')->nullable();
            $table->text('remarks');


            /* $table->tinyInteger('category_id');

            $table->string('category_time')->nullable();
            $table->string('category_resolution')->nullable();
            $table->integer('category_document_file_id')->nullable();

            $table->string('doc_type');
            $table->text('doc_description');
            $table->string('agenda');
            $table->integer('resolution_number');
            $table->text('resolution_subject');

            $table->integer('minute_file_id')->nullable();
            $table->integer('attendance_file_id')->nullable();
            
            $table->integer('memo_number');
            $table->string('memo_subject');
            $table->integer('memo_file_id')->nullable();
            
            $table->tinyInteger('quarter');
            $table->integer('document_file_id')->nullable();
            
            $table->text('document_description');
            $table->text('remarks'); */
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        Schema::dropIfExists('meeting_details');
    }
};
