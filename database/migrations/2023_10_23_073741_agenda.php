<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('agenda', function(Blueprint $table){
            $table->id();
            $table->date('meeting_date');
            $table->time('meeting_time');
            $table->integer('category');
            $table->text('document_description');
            $table->integer('quarter');
            $table->string('remarks');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
        Schema::dropIfExists('agenda');
    }
};

