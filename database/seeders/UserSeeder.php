<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //dd('test');
        DB::table('users')->insert([
            'role' => 'admin',
            'fullname' => 'Administrator1',
            'username' => 'admin',
            'password' => Hash::make('password'),
            'status' => true,
        ]);
        DB::table('users')->insert([
            'role' => 'superadmin',
            'fullname' => 'Superadmin',
            'username' => 'superadmin',
            'password' => Hash::make('password'),
            'status' => true,
        ]);
    }
}
