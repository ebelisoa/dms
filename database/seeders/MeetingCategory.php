<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MeetingCategory extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('category')->insert([
            'categoryName' => 'Admin Council'
        ]);
        DB::table('category')->insert([
            'categoryName' => 'Academic Council'
        ]);
        DB::table('category')->insert([
            'categoryName' => 'Preboard Meeting'
        ]);
        DB::table('category')->insert([
            'categoryName' => 'Regular Board Meeting'
        ]);
        DB::table('category')->insert([
            'categoryName' => 'Special Meeting'
        ]);
        
    }

}
